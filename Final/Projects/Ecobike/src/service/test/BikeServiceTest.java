//package service.test;
//
//import static org.junit.Assert.assertEquals;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.CsvSource;
//
//import service.BikeService;
//
//public class BikeServiceTest {
//	private BikeService bikeService;
//
//	@BeforeEach
//	public void initialize() {
//		this.bikeService = BikeService.getInstance();
//	}
//
//	@ParameterizedTest
//	@CsvSource({ "0, false", "000000, false", "aaa, false", "00000009, true" })
//	public void testBarcode(String barcode, boolean expected) {
//		boolean reality = bikeService.validateBarcode(barcode);
//		assertEquals(expected, reality);
//	}
//
//	@ParameterizedTest
//	@CsvSource({ "0, true", "null, true", ", false", "00000009, true" })
//	public void testName(String name, boolean expected) {
//		boolean reality = bikeService.validateName(name);
//		assertEquals(expected, reality);
//	}
//
//	@ParameterizedTest
//	@CsvSource({ "EBike, true", "TwinBike, true", "SingleBike, true", "Something else, false" })
//	public void testType(String type, boolean expected) {
//		boolean reality = bikeService.validateType(type);
//		assertEquals(expected, reality);
//	}
//
//	@ParameterizedTest
//	@CsvSource({ "0, false", "-1, false", "-3, false", "30, true" })
//	public void testWeight(int weight, boolean expected) {
//		boolean reality = bikeService.validateWeight(weight);
//		assertEquals(expected, reality);
//	}
//
//	@ParameterizedTest
//	@CsvSource({ "0, true", "000000, true", ", false" })
//	public void testLicensePlate(String licensePlate, boolean expected) {
//		boolean reality = bikeService.validateLicensePlate(licensePlate);
//		assertEquals(expected, reality);
//	}
//
//	@ParameterizedTest
//	@CsvSource({ "0, true", "000000, true", ", false" })
//	public void testProducer(String producer, boolean expected) {
//		boolean reality = bikeService.validateProducer(producer);
//		assertEquals(expected, reality);
//	}
//
//	@ParameterizedTest
//	@CsvSource({ "0, true", "-1, false"})
//	public void testCost(int barcode, boolean expected) {
//		boolean reality = bikeService.validateCost(barcode);
//		assertEquals(expected, reality);
//	}
//}
