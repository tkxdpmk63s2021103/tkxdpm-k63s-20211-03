package service;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.regex.Pattern;

import exceptions.AddBikeInitException;
import exceptions.ConflictBikeInfoException;
import exceptions.InvalidBikeInfoException;
import giaodichxe.model.Bike;
import repository.BikeRepository;

/**
 * Bike Service: Implement bike's service
 * 
 * @author Tinh
 *
 */
public class BikeService {
	private static BikeService BikeServiceInstance;

	private BikeService() {
	};

	public static BikeService getInstance() {
		if (BikeServiceInstance == null) {
			BikeServiceInstance = new BikeService();
		}
		return BikeServiceInstance;
	}

	public ArrayList<Bike> getBikeFromStation(int stationId) {
		return BikeRepository.getInstance().getBikeFromStation(stationId);
	}

	public Boolean deleteBikeFromBarcode(String barcode) {
		return BikeRepository.getInstance().deleteBikeFromBarcode(barcode);
	}

	public void unlockBike(String barcode) {
		BikeRepository.getInstance().unlockBike(barcode);
	}

	public void lockBike(String barcode, int stationId) {
		BikeRepository.getInstance().lockBike(barcode, stationId);
	}

	/**
	 * Get new barcode
	 * 
	 * @return {String} New barcode for new bike
	 * @throws SQLException
	 * @throws AddBikeInitException
	 */
	public String getNewBarcode() throws SQLException, AddBikeInitException {
		return BikeRepository.getInstance().getNewBarcode();
	}

	/**
	 * Get bike from barcode
	 * 
	 * @param barcode bike barcode
	 * @return
	 */
	public Bike getBikeFromBarcode(String barcode) {
		return BikeRepository.getInstance().getBikeFromBarcode(barcode);
	}

	/**
	 * Get all bike in database
	 * 
	 * @return {ArrayList<Bike>} Bike List
	 */
	public ArrayList<Bike> getAllBikes() {
		return BikeRepository.getInstance().getAllBikes();
	}

	/**
	 * Validate barcode, not null, length = 8, digits only
	 * 
	 * @param barcode Bike's barcode
	 * @return
	 */
	public boolean validateBarcode(String barcode) {
		if (barcode == null || barcode.length() != 8 || !Pattern.matches("\\d*", barcode))
			return false;
		return true;
	}

	/**
	 * Validate bike's cost, more than 0
	 * 
	 * @param cost
	 * @return
	 */
	public boolean validateCost(int cost) {
		return cost >= 0;
	}

	/**
	 * Validate license plate, not null or ""
	 * 
	 * @param licensePlate
	 * @return
	 */
	public boolean validateLicensePlate(String licensePlate) {
		if (licensePlate == null || licensePlate.length() == 0)
			return false;
		else
			return true;
	}

	/**
	 * Validate manufacturing date, not null or after today
	 * 
	 * @param manufacturingDate
	 * @return
	 */
	public boolean validateManufacturingDate(Date manufacturingDate) {
		if (manufacturingDate == null || LocalDate.now().compareTo(manufacturingDate.toLocalDate()) < 0)
			return false;
		else
			return true;
	}

	/**
	 * Validate name, not null or ""
	 * 
	 * @param name
	 * @return
	 */
	public boolean validateName(String name) {
		if (name == null || name.length() == 0)
			return false;
		else
			return true;
	}

	/**
	 * Validate weight, > 0
	 * 
	 * @param weight
	 * @return
	 */
	public boolean validateWeight(int weight) {
		return weight > 0;
	}

	public boolean validateProducer(String producer) {
		if (producer == null || producer.length() == 0)
			return false;
		else
			return true;
	}

	/**
	 * Validate bike type, not null, EBike, TwinBike or SingleBike
	 * 
	 * @param type
	 * @return
	 */
	public boolean validateType(String type) {
		if (type == null || (type.compareTo("EBike") != 0 && type.compareTo("TwinBike") != 0
				&& type.compareTo("SingleBike") != 0))
			return false;
		else
			return true;
	}

	public boolean validateStationId(int stationId) {
		if (stationId <= 0)
			return false;
		else
			return true;
	}

	/**
	 * Validate bike's basic info
	 * 
	 * @param bike
	 * @return true - Valid<br>
	 *         false - Invalid
	 * @throws Exception
	 */
	public boolean validateBasicBikeInfo(Bike bike) throws InvalidBikeInfoException {
		if (!validateBarcode(bike.getBarcode()))
			throw new InvalidBikeInfoException("Barcode invalid");
		if (!validateCost(bike.getCost()))
			throw new InvalidBikeInfoException("Bike cost need to more than 0");
		if (!validateLicensePlate(bike.getLicensePlate()))
			throw new InvalidBikeInfoException("License plate is required");
		if (!validateManufacturingDate(bike.getManufacturingDate()))
			throw new InvalidBikeInfoException("Invalid manufacturing date, can not be later than today");
		if (!validateName(bike.getName()))
			throw new InvalidBikeInfoException("Invalid bike name");
		if (!validateWeight(bike.getWeight()))
			throw new InvalidBikeInfoException("Bike weight must be more than 0");
		if (!validateProducer(bike.getProducer()))
			throw new InvalidBikeInfoException("Producer is required");
		if (!validateType(bike.getType()))
			throw new InvalidBikeInfoException("Invalid bike type");
		if (!validateStationId(bike.getStationId()))
			throw new InvalidBikeInfoException("Cannot rent");
		return true;
	}

	/**
	 * Check if any bike with same license plate with edited bike, that not the
	 * original bike
	 * 
	 * @param barcode      Bike barcode
	 * @param licensePlate Bike license plate
	 * @return
	 * @throws SQLException
	 */
	public boolean isLicensePlateDuplicate(String barcode, String licensePlate) throws SQLException {
		return BikeRepository.getInstance().isLicensePlateDuplicate(barcode, licensePlate);
	}

	/**
	 * Check if bike's stationId = NULL
	 * 
	 * @param barcode bike's barcode
	 * @return
	 * @throws SQLException
	 */
	public boolean isRenting(String barcode) throws SQLException {
		return BikeRepository.getInstance().isRenting(barcode);
	}

	/**
	 * Validate temporal bike info with database query
	 * 
	 * @return
	 * @throws SQLException
	 * @throws ConflictBikeInfoException
	 * 
	 */
	public boolean validateTemporalEditedBikeInfo(Bike bike, int oldStationId)
			throws ConflictBikeInfoException, SQLException {
		// Validate license plate
		if (BikeRepository.getInstance().isLicensePlateDuplicate(bike.getBarcode(), bike.getLicensePlate()))
			throw new ConflictBikeInfoException("Duplicated license plate");
		// Validate bike new bike station if any empty decks left
		BikeRepository.getInstance().isValidStation(bike.getStationId(), oldStationId);
		// Check if bike is renting
		if (BikeRepository.getInstance().isRenting(bike.getBarcode()))
			throw new ConflictBikeInfoException("Bike is rented");
		return true;
	}

	/**
	 * Save bike info
	 * 
	 * @param bike
	 * @return
	 * @throws SQLException
	 * @throws InvalidBikeInfoException
	 * @throws ConflictBikeInfoException
	 */
	public int saveBike(Bike bike, int oldStationId)
			throws SQLException, InvalidBikeInfoException, ConflictBikeInfoException {
		// Validate
		getInstance().validateBasicBikeInfo(bike);
		getInstance().validateTemporalEditedBikeInfo(bike, oldStationId);
		// Store
		return BikeRepository.getInstance().saveBike(bike);
	}

	/**
	 * Validate new bike info with database query
	 * 
	 * @param bike Bike info
	 * @return
	 * @throws ConflictBikeInfoException
	 * @throws SQLException
	 */
	public boolean validateTemporalNewBikeInfo(Bike bike) throws ConflictBikeInfoException, SQLException {
		// Check if license plate duplicate
		if (BikeRepository.getInstance().isLicensePlateDuplicate(bike.getBarcode(), bike.getLicensePlate()))
			throw new ConflictBikeInfoException("Duplicate license plate");
		// Check if station valid
		BikeRepository.getInstance().isValidStation(bike.getStationId(), 0);
		return true;
	}

	/**
	 * Add new bike in database
	 * 
	 * @param bike Add new bike in database
	 * @return
	 * @throws SQLException
	 * @throws InvalidBikeInfoException
	 * @throws ConflictBikeInfoException
	 */
	public int addBike(Bike bike) throws SQLException, InvalidBikeInfoException, ConflictBikeInfoException {
		// Check basic bike info
		getInstance().validateBasicBikeInfo(bike);
		// Check if bike info is invalid base using database query
		getInstance().validateTemporalNewBikeInfo(bike);
		// Check if barcode has been associated with any bike
		BikeRepository.getInstance().isBikeExist(bike.getBarcode());
		// Save bike
		return BikeRepository.getInstance().addBike(bike);
	}
}
