package service;

import java.util.ArrayList;

import repository.StationRepository;
import stationdetails.model.Station;

public class StationService {

	private static StationService stationServiceInstance;
	
	private StationService() {};
	
	public static StationService getInstance() {
		if (stationServiceInstance == null) {
			stationServiceInstance = new StationService();
		}
		return stationServiceInstance;
	}

	public Station getStationFromId(int stationId){
	    return StationRepository.getInstance().getStationFromId(stationId);
	}

	public ArrayList<Station> getListStation(){
	    return StationRepository.getInstance().getListStation();
	}

	public ArrayList<Station> searchStation(String input){
	    return StationRepository.getInstance().searchStation(input);
	}

	public void updateStationAfterRentBike(int stationId, String type){
	    StationRepository.getInstance().updateStationAfterRentBike(stationId, type);
	}

	public void updateStationAfterDelete(int stationId, String type){
		StationRepository.getInstance().updateStationAfterRentBike(stationId, type);
	}

	public void updateStationAfterReturnBike(int stationId, String type) {
	    StationRepository.getInstance().updateStationAfterReturnBike(stationId, type);
	}
}
