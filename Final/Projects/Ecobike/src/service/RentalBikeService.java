package service;

import giaodichxe.model.Bike;
import giaodichxe.model.RentalBike;
import repository.RentalBikeRepository;

public class RentalBikeService {
	
	private static RentalBikeService rentalBikeServiceInstance;
	
	private RentalBikeService() {};
	
	public static RentalBikeService getInstance() {
		if (rentalBikeServiceInstance == null) {
			rentalBikeServiceInstance = new RentalBikeService();
		}
		return rentalBikeServiceInstance;
	}

	public RentalBike getRentalBikeFromBarcode(String barcode) {
	    return RentalBikeRepository.getInstance().getRentalBikeFromBarcode(barcode);
	}

	public void saveRentalBike(Bike bike,int cardId){
	    RentalBikeRepository.getInstance().saveRentalBike(bike, cardId);
	}

	public void deleteRentalBike(Bike bike) {
	    RentalBikeRepository.getInstance().deleteRentalBike(bike);
	}

}
