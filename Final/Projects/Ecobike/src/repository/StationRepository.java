package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;

import stationdetails.model.Station;

public class StationRepository {
	private static StationRepository stationRepositoryInstance;
	private static SQLServerDataSource ds;
	
	private StationRepository() {};
	
	public static StationRepository getInstance() {
		if (stationRepositoryInstance == null) {
			ds = new SQLServerDataSource();
			ds.setUser("sa");
	        ds.setPassword("Meyeuthuong123");
	        ds.setServerName("localhost");
	        ds.setPortNumber(1433);
	        ds.setDatabaseName("Ecobike");
			stationRepositoryInstance = new StationRepository();
		}
		return stationRepositoryInstance;
	}
	
	public Station getStationFromId(int stationId){
	    Station station = null;
	    try {
	        Connection cnn = ds.getConnection();
	        Statement statement = cnn.createStatement();
	        ResultSet resultSet = statement.executeQuery("select * from station where stationId ='"+stationId+"'");
	        while (resultSet.next()) {
	            //int stationId, String stationName, String stationAddress, int numberOfSingleBike, int numberOfEBike, int numberOfTwinBike, int numberOfEmptyDocks
	            station = new Station(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3),resultSet.getInt(4),resultSet.getInt(5),resultSet.getInt(6),resultSet.getInt(7));
	        }
	    } catch (SQLServerException throwables) {
	        throwables.printStackTrace();
	    } catch (SQLException throwables) {
	        throwables.printStackTrace();
	    }
	    return station;
	}
	
	public ArrayList<Station> getListStation(){
	    ArrayList<Station> stations = new ArrayList<>();
	    try {
	        Connection cnn = ds.getConnection();
	        Statement statement = cnn.createStatement();
	        ResultSet resultSet = statement.executeQuery("select * from station");
	        while (resultSet.next()) {
	            //int stationId, String stationName, String stationAddress, int numberOfSingleBike, int numberOfEBike, int numberOfTwinBike, int numberOfEmptyDocks
	            Station station = new Station(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3),resultSet.getInt(4),resultSet.getInt(5),resultSet.getInt(6),resultSet.getInt(7));
	            stations.add(station);
	        }
	    } catch (SQLServerException throwables) {
	        throwables.printStackTrace();
	    } catch (SQLException throwables) {
	        throwables.printStackTrace();
	    }
	    return stations;
	}
	
	public ArrayList<Station> searchStation(String input){
	    ArrayList<Station> stations = new ArrayList<>();
	    try {
	        Connection cnn = ds.getConnection();
	        Statement statement = cnn.createStatement();
	        ResultSet resultSet = statement.executeQuery("select * from station where stationName like N'%" + input + "%' or stationAddress like N'%" + input + "%'");
	        while (resultSet.next()) {
	            //int stationId, String stationName, String stationAddress, int numberOfSingleBike, int numberOfEBike, int numberOfTwinBike, int numberOfEmptyDocks
	            Station station = new Station(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3),resultSet.getInt(4),resultSet.getInt(5),resultSet.getInt(6),resultSet.getInt(7));
	            stations.add(station);
	        }
	    } catch (SQLServerException throwables) {
	        throwables.printStackTrace();
	    } catch (SQLException throwables) {
	        throwables.printStackTrace();
	    }
	    return stations;
	}
	
	public void updateStationAfterRentBike(int stationId, String type){
	    try {
	        Connection cnn = Station.ds.getConnection();
	        PreparedStatement preparedStatement = cnn.prepareStatement("update station set numberOf"+type+" = numberOf"+type+" - 1, numberOfEmptyDocks = numberOfEmptyDocks + 1 where stationId =  "+stationId);
	        preparedStatement.execute();
	    } catch (SQLServerException throwables) {
	        throwables.printStackTrace();
	    } catch (SQLException throwables) {
	        throwables.printStackTrace();
	    }
	}
	
	public void updateStationAfterReturnBike(int stationId, String type) {
	    try {
	        Connection cnn = ds.getConnection();
	        PreparedStatement preparedStatement = cnn.prepareStatement("update station set numberOf"+type+" = numberOf"+type+" + 1, numberOfEmptyDocks = numberOfEmptyDocks - 1 where stationId =  "+stationId);
	        preparedStatement.execute();
	    } catch (SQLServerException throwables) {
	        throwables.printStackTrace();
	    } catch (SQLException throwables) {
	        throwables.printStackTrace();
	    }
	}
}
