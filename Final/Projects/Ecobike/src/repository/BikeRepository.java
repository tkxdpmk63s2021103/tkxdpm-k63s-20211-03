package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;

import exceptions.AddBikeInitException;
import exceptions.ConflictBikeInfoException;
import giaodichxe.model.Bike;
import giaodichxe.model.EBike;
import giaodichxe.model.SingleBike;
import giaodichxe.model.TwinBike;

public class BikeRepository {
	private static BikeRepository BikeRepositoryInstance;
	private static SQLServerDataSource ds;
	
	private BikeRepository() {};
	
	public static BikeRepository getInstance() {
		if (BikeRepositoryInstance == null) {
			ds = new SQLServerDataSource();
			ds.setUser("sa");
	        ds.setPassword("Meyeuthuong123");
	        ds.setServerName("localhost");
	        ds.setPortNumber(1433);
	        ds.setDatabaseName("Ecobike");
			BikeRepositoryInstance = new BikeRepository();
		}
		return BikeRepositoryInstance;
	}
	
	/**
	 * Get bike list from a station
	 * 
	 * @param stationId station Id
	 * @return {ArrayList<Bike>} Bike list
	 */
	public ArrayList<Bike> getBikeFromStation(int stationId) {
		ArrayList<Bike> bikes = new ArrayList<>();
		Bike bike = null;
		try {
			Connection cnn = ds.getConnection();
			Statement statement = cnn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Bike WHERE stationId = '" + stationId + "'");

			while (resultSet.next()) {
				String typeBike = resultSet.getString(3);
				if (typeBike.equals("EBike")) {
					bike = new EBike();
				} else if (typeBike.equals("TwinBike")) {
					bike = new TwinBike();
				} else {
					bike = new SingleBike();
				}
				bike.setBarcode(resultSet.getString(1));
				bike.setName(resultSet.getString(2));
				bike.setWeight(resultSet.getInt(4));
				bike.setLicensePlate(resultSet.getString(5));
				bike.setManufacturingDate(resultSet.getDate(6));
				bike.setProducer(resultSet.getString(7));
				bike.setCost(resultSet.getInt(8));
				bike.setDescription(resultSet.getString(9));
				bike.setImgPath(resultSet.getString(10));
				bike.setStationId(resultSet.getInt(11));
				bikes.add(bike);
			}
		} catch (SQLServerException throwables) {
			throwables.printStackTrace();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return bikes;
	}
	
	/**
	 * Delete bike with barcode
	 * 
	 * @param barcode bike barcode
	 * @return
	 */
	public Boolean deleteBikeFromBarcode(String barcode) {
		try {
			Connection cnn = ds.getConnection();
			Statement statement = cnn.createStatement();
			String sql = "delete from bike where barcode = '" + barcode + "'";
			int rowChange = statement.executeUpdate(sql);
			if (rowChange > 0) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
	
	public void unlockBike(String barcode) {
		try {
			Connection cnn = ds.getConnection();
			PreparedStatement preparedStatement = cnn
					.prepareStatement("UPDATE Bike SET stationId = NULL WHERE barcode='" + barcode + "'");
			preparedStatement.execute();
		} catch (SQLServerException throwables) {
			throwables.printStackTrace();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
	}
	
	public void lockBike(String barcode, int stationId) {
		try {
			Connection cnn = ds.getConnection();
			PreparedStatement preparedStatement = cnn.prepareStatement(
					"update Bike set stationId = '" + stationId + "' where barcode = '" + barcode + "'");
			preparedStatement.execute();
		} catch (SQLServerException throwables) {
			throwables.printStackTrace();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
	}
	
	/**
	 * Get bike from barcode
	 * 
	 * @param barcode bike barcode
	 * @return
	 */
	public Bike getBikeFromBarcode(String barcode) {
		Bike bike = null;
		try {
			Connection cnn = ds.getConnection();
			Statement statement = cnn.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from bike where barcode = '" + barcode + "'");

			while (resultSet.next()) {
				String typeBike = resultSet.getString(3);
				if (typeBike.equals("EBike")) {
					bike = new EBike();
				} else if (typeBike.equals("TwinBike")) {
					bike = new TwinBike();
				} else {
					bike = new SingleBike();
				}
				bike.setBarcode(resultSet.getString(1));
				bike.setName(resultSet.getString(2));
				bike.setWeight(resultSet.getInt(4));
				bike.setLicensePlate(resultSet.getString(5));
				bike.setManufacturingDate(resultSet.getDate(6));
				bike.setProducer(resultSet.getString(7));
				bike.setCost(resultSet.getInt(8));
				bike.setDescription(resultSet.getString(9));
				bike.setImgPath(resultSet.getString(10));
				bike.setStationId(resultSet.getInt(11));
			}
		} catch (SQLServerException throwables) {
			throwables.printStackTrace();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return bike;
	}
	
	/**
	 * Get all bike in database
	 * 
	 * @return {ArrayList<Bike>} Bike List
	 */
	public ArrayList<Bike> getAllBikes() {
		ArrayList<Bike> bikes = new ArrayList<>();
		Bike bike = null;
		try {
			Connection cnn = ds.getConnection();
			Statement statement = cnn.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from bike");

			while (resultSet.next()) {
				String typeBike = resultSet.getString(3);
				if (typeBike.equals("EBike")) {
					bike = new EBike();
				} else if (typeBike.equals("TwinBike")) {
					bike = new TwinBike();
				} else {
					bike = new SingleBike();
				}
				bike.setBarcode(resultSet.getString(1));
				bike.setName(resultSet.getString(2));
				bike.setWeight(resultSet.getInt(4));
				bike.setLicensePlate(resultSet.getString(5));
				bike.setManufacturingDate(resultSet.getDate(6));
				bike.setProducer(resultSet.getString(7));
				bike.setCost(resultSet.getInt(8));
				bike.setDescription(resultSet.getString(9));
				bike.setImgPath(resultSet.getString(10));
				bike.setStationId(resultSet.getInt(11));
				bikes.add(bike);
			}
		} catch (SQLServerException throwables) {
			throwables.printStackTrace();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return bikes;
	}
	
	/**
	 * Check if any bike with same license plate with edited bike, that not the
	 * original bike
	 * 
	 * @param barcode      Bike barcode
	 * @param licensePlate Bike license plate
	 * @return
	 * @throws SQLException
	 */
	public boolean isLicensePlateDuplicate(String barcode, String licensePlate) throws SQLException {
		Connection cnn = Bike.ds.getConnection();
		Statement statement = cnn.createStatement();
		ResultSet resultSet = statement.executeQuery(
				"SELECT * FROM BIKE WHERE licensePlate='" + licensePlate + "' AND NOT barcode='" + barcode + "'");
		// If found, resultSet.next() return true
		return resultSet.next();
	}
	
	/**
	 * Check if bike's stationId = NULL
	 * 
	 * @param barcode bike's barcode
	 * @return
	 * @throws SQLException
	 */
	public boolean isRenting(String barcode) throws SQLException {
		Connection cnn = Bike.ds.getConnection();
		Statement statement = cnn.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT stationId FROM BIKE WHERE barcode='" + barcode + "'");
		if (!resultSet.next())
			throw new SQLException("Bike's barcode not found");
		else {
			int stationId = resultSet.getInt("stationId");
			if (stationId == 0)
				return true;
			else
				return false;
		}
	}
	
	/**
	 * Check if barcode has been used
	 * 
	 * @param barcode Bike's barcode
	 * @return
	 * @throws SQLException
	 * @throws ConflictBikeInfoException
	 */
	public boolean isBikeExist(String barcode) throws SQLException, ConflictBikeInfoException {
		Connection cnn = Bike.ds.getConnection();
		Statement statement = cnn.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT * FROM BIKE WHERE barcode='" + barcode + "'");
		if (resultSet.next())
			throw new ConflictBikeInfoException("Barcode has been associated with other bike, please try again");
		return true;
	}
	
	/**
	 * Check if station exist and has any empty decks left?
	 * 
	 * @param stationId    New station Id
	 * @param oldStationId Old station Id, in case of add new bike, pass 0
	 * @return
	 * @throws SQLException
	 */
	public boolean isValidStation(int stationId, int oldStationId) throws ConflictBikeInfoException, SQLException {
		Connection cnn = Bike.ds.getConnection();
		Statement statement = cnn.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT * FROM station WHERE stationId=" + stationId);
		if (!resultSet.next())
			throw new ConflictBikeInfoException("Station doesn't exist");
		else if (stationId != oldStationId && resultSet.getInt("numberOfEmptyDocks") < 1) {
			throw new ConflictBikeInfoException("No more empty decks in this station");
		}
		return true;
	}
	
	/**
	 * Save bike info
	 * 
	 * @param bike
	 * @return
	 * @throws SQLException
	 */
	public int saveBike(Bike bike) throws SQLException {
		Connection cnn = Bike.ds.getConnection();
		Statement statement = cnn.createStatement();
		String query = "UPDATE BIKE SET name='" + bike.getName() + "',type='" + bike.getType() + "',weight="
				+ bike.getWeight() + ",licensePlate='" + bike.getLicensePlate() + "',manuafacturingDate='"
				+ bike.getManufacturingDate().toString() + "',producer='" + bike.getProducer() + "',cost="
				+ bike.getCost() + ", description='" + bike.getDescription() + "', stationId=" + bike.getStationId()
				+ " WHERE barcode='" + bike.getBarcode() + "'";
		// System.out.println(query);
		int result = statement.executeUpdate(query);
		return result;
	}
	
	/**
	 * Get new barcode  
	 * @return {String} New barcode for new bike
	 * @throws SQLException
	 * @throws AddBikeInitException
	 */
	public String getNewBarcode() throws SQLException, AddBikeInitException {
		Connection cnn = Bike.ds.getConnection();
		Statement statement = cnn.createStatement();
		String query = "SELECT TOP 1 * FROM Bike ORDER BY barcode DESC";
		ResultSet resultSet = statement.executeQuery(query);
		if (!resultSet.next())
			return "00000001";
		else {
			String biggestBikeBarcode = resultSet.getString("barcode");
			int biggestBikeBarcodeInt = Integer.parseInt(biggestBikeBarcode);
			if (biggestBikeBarcodeInt >= 99999999)
				throw new AddBikeInitException("Number of bike reach limit");
			Integer newBikeBarcodeInt = biggestBikeBarcodeInt + 1;
			int countChar = newBikeBarcodeInt.toString().length();
			String newBikeBarcode = "";
			while (8 - countChar > 0) {
				newBikeBarcode += "0";
				countChar++;
			}
			return newBikeBarcode + newBikeBarcodeInt;
		}
	}
	
	/**
	 * Add new bike in database
	 * 
	 * @param bike Add new bike in database
	 * @return
	 * @throws SQLException
	 */
	public int addBike(Bike bike) throws SQLException {
		Connection cnn = Bike.ds.getConnection();
		Statement statement = cnn.createStatement();
		String query = "INSERT INTO Bike (barcode, name, type, weight, licensePlate, manuafacturingDate, producer, cost, description, stationId) VALUES ('"
				+ bike.getBarcode() + "', '" + bike.getName() + "', '" + bike.getType() + "', " + bike.getWeight()
				+ ", '" + bike.getLicensePlate() + "', '" + bike.getManufacturingDate().toString() + "', '"
				+ bike.getProducer() + "', " + bike.getCost() + ", '" + bike.getDescription() + "', '"
				+ bike.getStationId() + "')";
		// System.out.println(query);
		int result = statement.executeUpdate(query);
		return result;
	}
}
