package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;

import giaodichxe.model.Bike;
import giaodichxe.model.RentalBike;
import service.BikeService;

public class RentalBikeRepository {
	private static RentalBikeRepository rentalBikeRepositoryInstance;
	
	private static SQLServerDataSource ds;
	
	private RentalBikeRepository() {};
	
	public static RentalBikeRepository getInstance() {
		if (rentalBikeRepositoryInstance == null) {
			ds = new SQLServerDataSource();
			ds.setUser("sa");
	        ds.setPassword("Meyeuthuong123");
	        ds.setServerName("localhost");
	        ds.setPortNumber(1433);
	        ds.setDatabaseName("Ecobike");
			rentalBikeRepositoryInstance = new RentalBikeRepository();
		}
		return rentalBikeRepositoryInstance;
	}
	
	public RentalBike getRentalBikeFromBarcode(String barcode) {
	    RentalBike rentalBike = new RentalBike();
	    try {
	        Connection cnn = ds.getConnection();
	        Statement statement = cnn.createStatement();
	        ResultSet resultSet = statement.executeQuery("select * from RentalBike where barcode = '"+barcode+"' and dateReturnBike is null");
	
	        while (resultSet.next()) {
	           rentalBike.setBike(BikeService.getInstance().getBikeFromBarcode(resultSet.getString(2)));
	           rentalBike.setCardId(Integer.valueOf(resultSet.getString(1)));
	            int year = resultSet.getDate(3).getYear();
	            int month = resultSet.getDate(3).getMonth();
	            int day = resultSet.getDate(3).getDate();
	            int h = resultSet.getTime(3).getHours();
	            int p = resultSet.getTime(3).getMinutes();
	            int giay = resultSet.getTime(3).getSeconds();
	            Date date = new Date(year,month,day,h,p,giay);
	            rentalBike.setStartTime(date);
	            rentalBike.setEndTime(resultSet.getDate(4));
	        }
	    } catch (SQLServerException throwables) {
	        throwables.printStackTrace();
	    } catch (SQLException throwables) {
	        throwables.printStackTrace();
	    }
	    return rentalBike;
	}


	public void saveRentalBike(Bike bike,int cardId){
	    //Lưu RentalBike
	    try (Connection connection = ds.getConnection()) {
	        PreparedStatement preparedStatement = connection.prepareStatement("insert into RentalBike (carId,barcode,dateRentBike) values(?,?,?)");
	        preparedStatement.setString(1, String.valueOf(cardId));
	        preparedStatement.setString(2, bike.getBarcode());
	        Date date = new Date();
	        int year = date.getYear() + 1900;
	        int month = date.getMonth() + 1;
	        int day = date.getDate();
	        System.out.println();
	        preparedStatement.setString(3,year+"-"+month+"-"+day+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+".000" );
	        preparedStatement.execute();
	    } catch (SQLServerException throwables) {
	        throwables.printStackTrace();
	    } catch (SQLException throwables) {
	        throwables.printStackTrace();
	    }
	}
	
	public void deleteRentalBike(Bike bike) {
	    try {
	        Connection cnn = ds.getConnection();
	        PreparedStatement preparedStatement = cnn.prepareStatement("delete from RentalBike where barcode = '"+bike.getBarcode()+"'");
	        preparedStatement.execute();
	    } catch (SQLServerException throwables) {
	        throwables.printStackTrace();
	    } catch (SQLException throwables) {
	        throwables.printStackTrace();
	    }
	}
}


