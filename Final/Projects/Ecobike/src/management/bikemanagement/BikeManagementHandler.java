package management.bikemanagement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import giaodichxe.model.Bike;
import stationdetails.model.Station;
import stationdetails.view.viewhome.HomeHandle;
import view.BaseHandler;
import management.bikemanagement.bikeadd.BikeAddHandler;
import management.bikemanagement.bikeeditor.BikeEditorHandler;
import service.BikeService;
import service.StationService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class BikeManagementHandler extends BaseHandler {
	
	
	public BikeManagementHandler(String fxmlPath) throws IOException {
		super(fxmlPath);
		this.setData();
	}
	
	public BikeManagementHandler(Stage stage, String fxmlPath) throws IOException {
		super(stage, fxmlPath);
		this.setData();
	}

	@FXML
	private TextField searchInput;

	@FXML
	private Button searchBtn;

	@FXML
	private Button addBtn;

	@FXML
	private TableView<Bike> tableView;

	@FXML
	TableColumn<Bike, String> barcode;

	@FXML
	TableColumn<Bike, String> bikeName;

	@FXML
	TableColumn<Bike, String> type;

	@FXML
	TableColumn<Bike, String> licensePlate;

	@FXML
	TableColumn<Bike, String> producer;
	
	@FXML
	TableColumn<Bike, String> stationName;

	private ObservableList<Bike> bikesOb;
	/**
	 * Get and render bike list data
	 */
	public void setData() {
		try {
			// Get station list
			ArrayList<Station> stations = StationService.getInstance().getListStation();
			// Create station map
			HashMap<Integer, String> stationMap = new HashMap<Integer, String>();
			stations.forEach(e -> {
				stationMap.put(e.getStationId(), e.getStationName());
			});
			// Add station name to bike
			ArrayList<Bike> bikes = BikeService.getInstance().getAllBikes();
			bikes.forEach(e -> {
				int stationId = e.getStationId();
				e.setStationName(stationId != 0 ? stationMap.get(e.getStationId()) : "Rented");
			});
			// Render table
			bikesOb = FXCollections.observableArrayList(bikes);

			barcode.setCellValueFactory(new PropertyValueFactory<Bike, String>("barcode"));
			bikeName.setCellValueFactory(new PropertyValueFactory<Bike, String>("name"));
			type.setCellValueFactory(new PropertyValueFactory<Bike, String>("type"));
			licensePlate.setCellValueFactory(new PropertyValueFactory<Bike, String>("licensePlate"));
			producer.setCellValueFactory(new PropertyValueFactory<Bike, String>("producer"));
			stationName.setCellValueFactory(new PropertyValueFactory<Bike, String>("stationName"));

			tableView.setItems(bikesOb);
			
			addBtn.setOnMouseClicked(e -> addBike(e));
		} catch (Exception e) {
			this.showAlert("Can't not show bike list");
		}
	}

	/**
	 * Display a bike information
	 * 
	 * @param mouseEvent
	 */
	public void selectBike(MouseEvent mouseEvent) {
		try {
			if (tableView.getSelectionModel().getSelectedItem() == null) return;
			Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
			BaseHandler handler = new BikeEditorHandler(stage, "/management/bikemanagement/bikeeditor/BikeEditor.fxml",
					tableView.getSelectionModel().getSelectedItem());
			handler.show();
		} catch (Exception e) {
			e.printStackTrace();
			this.showAlert("Can not display bike information.");
			this.setData();
		}
	}
	
	/**
	 * Show add bike screen
	 * @param mouseEvent
	 */
	public void addBike(MouseEvent mouseEvent) {
		try {
			Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
			BaseHandler handler = new BikeAddHandler(stage, "/management/bikemanagement/bikeadd/BikeAdd.fxml");
			handler.show();
		} catch (Exception e) {
			e.printStackTrace();
			this.showAlert("Somethings went wrong, can not open add new bike window.");
			this.setData();
		}
	}

	/**
	 * Return to home scene
	 * 
	 * @param event Event
	 * @throws IOException
	 */
	public void back(ActionEvent event) throws IOException {
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		BaseHandler handler = new HomeHandle(stage, "/stationdetails/view/viewhome/HomeScreen.fxml");
		handler.show();
	}
	
	protected void showAlert(String messgae) {
		super.showAlert("INFORMATION", messgae);
	}
}
