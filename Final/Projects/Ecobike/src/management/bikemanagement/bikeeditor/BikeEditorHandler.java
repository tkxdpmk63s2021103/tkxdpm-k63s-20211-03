package management.bikemanagement.bikeeditor;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

import exceptions.ConflictBikeInfoException;
import exceptions.InvalidBikeInfoException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import giaodichxe.model.Bike;
import view.BaseHandler;
import service.BikeService;
import service.StationService;
import stationdetails.model.Station;
import management.bikemanagement.BikeManagementHandler;

public class BikeEditorHandler extends BaseHandler {

	public BikeEditorHandler(Stage stage, String fxmlPath) throws IOException {
		super(stage, fxmlPath);
	}

	public BikeEditorHandler(Stage stage, String fxmlPath, Bike bike) throws IOException {
		super(stage, fxmlPath);
		this.setData(bike);
		this.saveBtn.setOnMouseClicked(event -> {
			try {
				saveBike();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		this.refreshBtn.setOnMouseClicked(event -> {
			try {
				refresh();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	@FXML
	Label barcode;

	@FXML
	TextField name;

	@FXML
	ComboBox<String> type;

	@FXML
	ComboBox<String> stationId;

	@FXML
	TextField weight;

	@FXML
	TextField licensePlate;

	@FXML
	DatePicker manufacturingDate;

	@FXML
	TextField producer;

	@FXML
	TextField cost;

	@FXML
	TextArea description;

	@FXML
	ImageView viewBike;

	@FXML
	Button saveBtn;

	@FXML
	Button refreshBtn;

	Bike selectedBike;
	HashMap<String, Integer> stationOptions;
	int oldStationId;
	String oldType;

	public void setData(Bike bike) throws IOException {
		try {
			this.selectedBike = bike;
			this.oldStationId = this.selectedBike.getStationId();
			this.oldType = this.selectedBike.getType();

			System.out.println("Details : " + bike.getBarcode());
			// Open control
			this.setDisableControl(false);
			// Get current stationId
			int currentStationId = bike.getStationId();
			// If bike is rented, set "Rented"
			// disable control
			if (currentStationId == 0) {
				stationId.getItems().add("Rented");
				stationId.setValue("Rented");
				this.setDisableControl(true);
			} else {
				// Get station list
				ArrayList<Station> stations = StationService.getInstance().getListStation();
				this.stationOptions = new HashMap<String, Integer>();
				// Clear first
				stationId.getItems().clear();

				// Fill option map
				stations.forEach(e -> {
					stationOptions.put(e.getStationName(), e.getStationId());
					stationId.getItems().add(e.getStationName());
					if (currentStationId == e.getStationId())
						stationId.setValue(e.getStationName());
				});
			}
			

			// Fill form
			barcode.setText(bike.getBarcode());
			name.setText(bike.getName());
			// Clear option
			type.getItems().clear();
			type.getItems().addAll("EBike", "TwinBike", "SingleBike");
			type.setValue(bike.getType());
			weight.setText(String.valueOf(bike.getWeight()));
			licensePlate.setText(bike.getLicensePlate());
			manufacturingDate.setValue(LOCAL_DATE(String.valueOf(bike.getManufacturingDate())));
			producer.setText(bike.getProducer());
			cost.setText(String.valueOf(bike.getCost()));
			description.setText(bike.getDescription());
			File file = new File(bike.getImgPath());
			Image image = new Image(file.toURI().toURL().toString());
			viewBike.setImage(image);

		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			this.showAlert("Can't get bike information");
			Stage stage = (Stage) barcode.getScene().getWindow();
			// Return to bike management
			this.backToBikeManagement(stage);
			return;
		}
	}

	/**
	 * Save changed bike infomation
	 * 
	 * @throws IOException
	 */
	public void saveBike() throws IOException {
		try {
			this.setDisableControl(true);
			// Get data from form
			this.selectedBike.setName(name.getText());
			this.selectedBike.setType(type.getValue().toString());
			this.selectedBike.setLicensePlate(licensePlate.getText());
			this.selectedBike.setWeight(Integer.parseInt(weight.getText()));
			this.selectedBike.setManufacturingDate(Date.valueOf(manufacturingDate.getValue()));
			this.selectedBike.setProducer(producer.getText());
			this.selectedBike.setDescription(description.getText());
			this.selectedBike.setCost(Integer.parseInt(cost.getText()));
			this.selectedBike.setStationId(this.stationOptions.get(stationId.getValue()));

			// Save
			if (BikeService.getInstance().saveBike(selectedBike, oldStationId) == 1) {
				// Except for when keeping both station and type
				// Remove a old type bike from old station
				// Add a new type bike to new station
				if (this.oldStationId != this.selectedBike.getStationId()
						|| this.oldType != this.selectedBike.getType()) {
					StationService.getInstance().updateStationAfterRentBike(this.oldStationId, this.oldType);
					StationService.getInstance().updateStationAfterReturnBike(this.selectedBike.getStationId(), this.selectedBike.getType());
				}
				this.showAlert("Success");
			} else
				this.showAlert("Failed");
			this.setDisableControl(false);
		} catch (InvalidBikeInfoException e) {
			this.showAlert(e.getMessage());
			this.setDisableControl(false);
			return;
		} catch (ConflictBikeInfoException e) {
			this.showAlert(e.getMessage());
			this.setDisableControl(false);
			return;
		} catch (NumberFormatException e) {
			this.showAlert("Fields are empty or wrong data type");
			this.setDisableControl(false);
			return;
		} catch (SQLException e) {
			e.printStackTrace();
			this.showAlert("Error: Query database error.");
			Stage stage = (Stage) barcode.getScene().getWindow();
			// return to bike management
			this.backToBikeManagement(stage);
			return;
		}
	}

	public void refresh() throws IOException {
		selectedBike = BikeService.getInstance().getBikeFromBarcode(selectedBike.getBarcode());
		try {
			this.setData(selectedBike);
		} catch (IOException e) {
			this.showAlert("Error: Query database error.");
			Stage stage = (Stage) barcode.getScene().getWindow();
			// return to bike management
			this.backToBikeManagement(stage);
			return;
		}
	}

	/**
	 * return to bike management
	 * 
	 * @param actionEvent
	 * @throws IOException
	 */
	public void backToBikeManagement(ActionEvent actionEvent) throws IOException {
		Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
		BaseHandler handler = new BikeManagementHandler(stage, "/management/bikemanagement/BikeManagement.fxml");
		handler.show();
	}

	/**
	 * Return to bike management
	 * 
	 * @param stage
	 * @throws IOException
	 */
	public void backToBikeManagement(Stage stage) throws IOException {
		BaseHandler handler = new BikeManagementHandler(stage, "/management/bikemanagement/BikeManagement.fxml");
		handler.show();
	}

	/**
	 * return local date in yyyy-MM-dd
	 * 
	 * @param dateString
	 * @return LocalDate
	 */
	public static final LocalDate LOCAL_DATE(String dateString) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localDate = LocalDate.parse(dateString, formatter);
		return localDate;
	}

	/**
	 * Open control
	 * 
	 * @param isDisable
	 */
	protected void setDisableControl(boolean isDisable) {
		name.setDisable(isDisable);
		type.setDisable(isDisable);
		cost.setDisable(isDisable);
		stationId.setDisable(isDisable);
		weight.setDisable(isDisable);
		licensePlate.setDisable(isDisable);
		manufacturingDate.setDisable(isDisable);
		producer.setDisable(isDisable);
		description.setDisable(isDisable);
		saveBtn.setDisable(isDisable);
	}

	protected void showAlert(String message) {
		super.showAlert("INFORMATION", message);
	}
}
