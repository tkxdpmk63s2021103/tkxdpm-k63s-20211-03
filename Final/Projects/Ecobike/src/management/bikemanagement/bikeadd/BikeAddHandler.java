package management.bikemanagement.bikeadd;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

import exceptions.AddBikeInitException;
import exceptions.ConflictBikeInfoException;
import exceptions.InvalidBikeInfoException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import giaodichxe.model.Bike;
import stationdetails.model.Station;
import view.BaseHandler;
import service.BikeService;
import service.StationService;
import management.bikemanagement.BikeManagementHandler;

public class BikeAddHandler extends BaseHandler {

	public BikeAddHandler(Stage stage, String fxmlPath) throws IOException {
		super(stage, fxmlPath);
		this.setData();
	}

	@FXML
	Label barcode;

	@FXML
	TextField name;

	@FXML
	ComboBox<String> type;

	@FXML
	ComboBox<String> stationId;

	@FXML
	TextField weight;

	@FXML
	TextField licensePlate;

	@FXML
	DatePicker manufacturingDate;

	@FXML
	TextField producer;

	@FXML
	TextField cost;

	@FXML
	TextArea description;

	@FXML
	ImageView viewBike;

	@FXML
	Button addBtn;

	Bike newBike;
	HashMap<String, Integer> stationOptions;

	public void setData() throws IOException {
		try {
			// Create new bike and get new barcode
			newBike = new Bike();
			String newBarcode = BikeService.getInstance().getNewBarcode();
			newBike.setBarcode(newBarcode);
			
			// Get station list
			ArrayList<Station> stations = StationService.getInstance().getListStation();
			this.stationOptions = new HashMap<String, Integer>();
			// Clear stationId
			stationId.getItems().clear();
			// Fill option map
			stations.forEach(e -> {
				stationOptions.put(e.getStationName(), e.getStationId());
				stationId.getItems().add(e.getStationName());
				stationId.setValue(e.getStationName());
			});

			// Fill form
			barcode.setText(newBike.getBarcode());
			name.setText("");
			type.getItems().clear();
			type.getItems().addAll("SingleBike", "EBike", "TwinBike");
			type.setValue("SingleBike");
			weight.setText("");
			licensePlate.setText("");
			manufacturingDate.setValue(LOCAL_DATE(LocalDate.now().toString()));
			producer.setText("");
			cost.setText("0");
			description.setText("");
			
			addBtn.setOnMouseClicked(event -> {
				try {
					addBike();
				} catch (IOException e) {
					e.printStackTrace();
				}
			});

		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (AddBikeInitException e) {
			e.printStackTrace();
			this.showAlert(e.getMessage());
			this.backToBikeManagement();
			return;
		} catch (Exception e) {
			e.printStackTrace();
			this.showAlert("Can't find bike information");
			// Return to bike management
			this.backToBikeManagement();
			return;
		}
	}

	/**
	 * add bike infomation
	 * 
	 * @throws IOException
	 */
	public void addBike() throws IOException {
		try {
			// Get data from form
			this.newBike.setName(name.getText());
			this.newBike.setName(name.getText());
			this.newBike.setType(type.getValue().toString());
			this.newBike.setLicensePlate(licensePlate.getText());
			this.newBike.setWeight(Integer.parseInt(weight.getText()));
			this.newBike.setManufacturingDate(Date.valueOf(manufacturingDate.getValue()));
			this.newBike.setProducer(producer.getText());
			this.newBike.setDescription(description.getText());
			this.newBike.setCost(Integer.parseInt(cost.getText()));
			this.newBike.setStationId(this.stationOptions.get(stationId.getValue()));

			if (BikeService.getInstance().addBike(newBike) == 1) {
				// Update station
				StationService.getInstance().updateStationAfterReturnBike(newBike.getStationId(), newBike.getType());
				this.showAlert("Success");
				this.setData();
				return;
			} else {
				this.showAlert("Failed");
				return;
			}
		} catch (InvalidBikeInfoException e) {
			this.showAlert(e.getMessage());
			return;
		} catch (ConflictBikeInfoException e) {
			this.showAlert(e.getMessage());
			return;
		} catch (NumberFormatException e) {
			this.showAlert("Fields are missing or wrong type");
			return;
		} catch (SQLException e) {
			e.printStackTrace();
			this.showAlert("Error: Database query error");
			// Return to bike management
			this.backToBikeManagement();
			return;
		}
	}

	/**
	 * Return to bike management
	 * 
	 * @param actionEvent
	 * @throws IOException
	 */
	public void backToBikeManagement(ActionEvent actionEvent) throws IOException {
		Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
		BaseHandler handler = new BikeManagementHandler(stage, "/management/bikemanagement/BikeManagement.fxml");
		handler.show();
	}

	/**
	 * Return to bike management
	 * 
	 * @param stage
	 * @throws IOException
	 */
	public void backToBikeManagement() throws IOException {
		BaseHandler handler = new BikeManagementHandler("/management/bikemanagement/BikeManagement.fxml");
		handler.show();
	}

	/**
	 * return LocalDate in yyyy-MM-dd
	 * 
	 * @param dateString
	 * @return LocalDate
	 */
	public static final LocalDate LOCAL_DATE(String dateString) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localDate = LocalDate.parse(dateString, formatter);
		return localDate;
	}

	/**
	 * Alert infor
	 */
	protected void showAlert(String message) {
		super.showAlert("INFORMATION", message);
	}
}
