package management.admin;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import management.bikemanagement.BikeManagementHandler;
import view.BaseHandler;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class AdminHandle extends BaseHandler {
	private static Boolean loginAsAdmin = false;



	public static Boolean hasLoginAsAdmin() {
		return loginAsAdmin;
	}
	
	@FXML
    private TextField AccountTextField;
	
	@FXML
    private PasswordField PasswordTextField;

	@FXML
	private Button loginBtn;

	public AdminHandle(String fxmlPath) throws IOException {
		super(fxmlPath);
		loginBtn.setOnMouseClicked(event -> {
			try {
				loginAdmin(event);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	public void loginAdmin(MouseEvent event) throws IOException {
		String account = AccountTextField.getText();
		String pass = PasswordTextField.getText();
		if(account == "" || pass == "") {
			showAlert("Dien thieu truong");
			return;
		}
		
		//query trong database
		if(Admin.authenticateAccount(account, pass)) {
			AdminHandle.loginAsAdmin = true;
			System.out.println(AdminHandle.loginAsAdmin);
			showAlert("Dang nhap thanh cong");
			//backToHome(event);
		}else {
			AdminHandle.loginAsAdmin = false;
			showAlert("Dang nhap tu choi");
		}
	}
	
	public void backToHome(ActionEvent event) throws IOException {
		switchBack();
    }
	
	public void handleReturnBikeClick(ActionEvent actionEvent) throws IOException {

    }
	
	public void handleBikeManageClick(MouseEvent mouseEvent) throws IOException {
        if (AdminHandle.hasLoginAsAdmin()) {
        	Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
        	BaseHandler handler = new BikeManagementHandler(stage, "/management/bikemanagement/BikeManagement.fxml");
            handler.show();
        } else {
        	this.showAlert("INFORMATION", "Can not access without logged in as admin!");
        }
    }
	
	private void showAlert(String text) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Admin Login");
		alert.setContentText(text);
		alert.showAndWait();
	}
}
