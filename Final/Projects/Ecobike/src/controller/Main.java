package controller;

import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import stationdetails.view.viewhome.HomeHandle;
import view.BaseHandler;

public class Main extends Application {

    public SQLServerDataSource getConnect(){
        SQLServerDataSource ds = new SQLServerDataSource();
        ds.setUser("sa");
        ds.setPassword("Meyeuthuong123");
        ds.setServerName("localhost");
        ds.setPortNumber(1433);
        ds.setDatabaseName("Ecobike");
        return ds;
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        BaseHandler handler = new HomeHandle(primaryStage,"/stationdetails/view/viewhome/HomeScreen.fxml");
        handler.show();
    }
}
