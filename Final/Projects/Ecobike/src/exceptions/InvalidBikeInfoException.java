package exceptions;

public class InvalidBikeInfoException extends Exception {
	public InvalidBikeInfoException(String message) {
		super(message);
	}
}
