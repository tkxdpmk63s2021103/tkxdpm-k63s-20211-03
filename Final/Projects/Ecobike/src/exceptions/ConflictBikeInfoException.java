package exceptions;

public class ConflictBikeInfoException extends Exception {
	public ConflictBikeInfoException(String message) {
		super(message);
	}
}
