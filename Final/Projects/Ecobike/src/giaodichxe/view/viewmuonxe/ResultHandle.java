package giaodichxe.view.viewmuonxe;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import stationdetails.view.viewhome.HomeHandle;
import view.BaseHandler;

public class ResultHandle extends BaseHandler {
    @FXML
    private Button buttonHome;

    public ResultHandle(String fxmlPath) {
        super(fxmlPath);
        buttonHome.setOnMouseClicked(event -> {
            try {
                handleHomeClick(event);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void handleHomeClick(MouseEvent event) throws IOException {
    	BaseHandler handler = new HomeHandle(getStage(), "/stationdetails/view/viewhome/HomeScreen.fxml");
    	switchTo(handler);
    	
    }

    public void handleHelpClick(ActionEvent event) {
    	
    }

    public void handleFeedbackClick(ActionEvent event) {
    	
    }


}
