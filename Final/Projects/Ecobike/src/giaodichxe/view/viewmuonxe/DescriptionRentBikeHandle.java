package giaodichxe.view.viewmuonxe;

import giaodichxe.model.Bike;
import giaodichxe.controller.RentBikeController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import payment.controller.PaymentConfig;
import payment.view.PaymentHandler;
import payment.view.PaymentResultInterface;
import view.BaseHandler;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;


public class DescriptionRentBikeHandle extends BaseHandler implements PaymentResultInterface{

    private Bike bike;
    private String description;


    private int deposit;
    @FXML
    private Label Deposit;

    @FXML
    private Label Description;

    @FXML
    private Label barcode;

    @FXML
    private Button confirm;

    @FXML
    private ImageView img;

    public void success(int cardId) {
        RentBikeController rentBikeController =  (RentBikeController) getController();
        rentBikeController.updateDataAfterRentBike(bike, cardId);
        BaseHandler handler = new ResultHandle("/giaodichxe/view/viewmuonxe/ResultRentBike.fxml");
        switchTo(handler);
    }

    public void setData(String message, Bike bike) throws MalformedURLException {
        try {
        	RentBikeController rentBikeController = (RentBikeController) getController();
            deposit = rentBikeController.calculateDeposit(bike.getType());
            Deposit.setText(String.valueOf(deposit));
            Description.setText(message);
            barcode.setText(bike.getBarcode());
            File file = new File(bike.getImgPath());
            Image image = new Image(file.toURI().toURL().toString());
            img.setImage(image);
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }

    public DescriptionRentBikeHandle(String fxmlPath, Bike bike, String description) throws IOException {
        super(fxmlPath);
        this.bike = bike;
        this.description = description;
        setController(new RentBikeController());
        confirm.setOnMouseClicked(event -> {
            try {
                changeScreenToPay(event);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        setData(description,bike);
    }




    public void displayError(String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText(message);
        alert.show();
    }

    public void changeScreenToPay(MouseEvent actionEvent) throws IOException {
    	BaseHandler paymentScene =  new PaymentHandler("/payment/view/fxml/PaymentScreen.fxml", "/payment/view/css/paymentStyle.css", PaymentConfig.PAY, deposit , "Giao dich thu tien xe bla bla");
        switchTo(paymentScene);
    }

    public void backToBikeDetails(ActionEvent event) throws IOException {
        switchBack();
    }
}
