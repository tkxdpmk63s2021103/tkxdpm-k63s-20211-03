package giaodichxe.view.viewmuonxe;


import giaodichxe.controller.DataController;
import giaodichxe.model.Bike;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import stationdetails.model.Station;
import view.BaseHandler;


import service.BikeService;
import service.StationService;
import management.admin.AdminHandle;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;



public class BikeDetailsHandle extends BaseHandler{
    @FXML
    Label Barcode;

    @FXML
    Label Name;

    @FXML
    Label Type;

    @FXML
    Label Weight;

    @FXML
    Label LicensePlate;

    @FXML
    Label ManufacturingDate;

    @FXML
    Label Producer;

    @FXML
    Label Cost;

    @FXML
    Label Description;

    @FXML
    ImageView ViewBike;
    
    @FXML
    Button rentBtn;
    
    @FXML
    Button delBtn;

    private DataController dataController = new DataController();

    public Bike bike;

    public BikeDetailsHandle(String fxmlPath,Bike bike) throws IOException {
        super(fxmlPath);
        this.bike = bike;
        setInfoBike(bike);
        rentBtn.setOnMouseClicked(e -> {
        	try {
				changeScreenDescriptionRentBike(e);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        });
        delBtn.setOnMouseClicked(e -> {
        	try {
				deleteBike(e);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        });
    }

    public void setInfoBike(Bike bike) throws MalformedURLException {
        try {
        	System.out.println(bike.getBarcode());
        	Barcode.setText(bike.getBarcode());
            Name.setText(bike.getName());
            Type.setText(bike.getType());
            Weight.setText(String.valueOf(bike.getWeight()));
            LicensePlate.setText(bike.getLicensePlate());
            ManufacturingDate.setText(String.valueOf(bike.getManufacturingDate()));
            Producer.setText(bike.getProducer());
            Cost.setText(String.valueOf(bike.getCost()));
            Description.setText(bike.getDescription());
            File file = new File(bike.getImgPath());
            Image image = new Image(file.toURI().toURL().toString());
            ViewBike.setImage(image);
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }


    public void backToStationDetails(MouseEvent actionEvent) throws IOException {
        switchBack();
    }


    public void changeScreenDescriptionRentBike(MouseEvent actionEvent) throws IOException {
        String desctipton  = "Với xe đạp đơn thường, nếu khách hàng dùng xe hơn 10 phút, phí thuê xe được tính lũy tiến\n" +
                "theo thời gian thuê như sau: Giá khởi điểm cho 30 phút đầu là 10.000 đồng. Cứ mỗi 15 phút\n" +
                "tiếp theo, khách sẽ phải trả thêm 3.000 đồng. Ví dụ, khách thuê 1 tiếng 10 phút cần trả 10.000 + 3x3.000 = 19.000 đồng.\n" +
                "Phí thuê xe đạp đơn điện và xe đạp đôi thường gấp 1.5 lần so với phí thuê xe đạp đơn thường.";
        BaseHandler handler = new DescriptionRentBikeHandle("/giaodichxe/view/viewmuonxe/DescriptionRentBike.fxml",bike,desctipton);
        switchTo(handler);
    }
    
    public void deleteBike(MouseEvent actionEvent) throws IOException {
    	//check if admin
    	if(!AdminHandle.hasLoginAsAdmin()) {
    		showAlert("Admin Authentication", "Ban khong co quyen admin");
    		return;
    	}

    	// delete record from database
    	if(BikeService.getInstance().deleteBikeFromBarcode(bike.getBarcode())){
    		showAlert("Bike Delete", "Xoa thanh cong");
            StationService.getInstance().updateStationAfterDelete(bike.getStationId(),bike.getType());
    		backToStationDetails(actionEvent);
    	}else {
    		showAlert("Bike Delete", "Xoa that bai");
    	}
    }
}
