package giaodichxe.view.viewtraxe;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import stationdetails.view.viewhome.HomeHandle;
import view.BaseHandler;

import java.io.IOException;

public class ResultReturnBikeHandle extends BaseHandler {
    @FXML
    Button buttonHome;

    public ResultReturnBikeHandle(String fxmlPath) {
        super(fxmlPath);
        buttonHome.setOnMouseClicked(event -> {
            try {
                handleHomeClick(event);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void handleHomeClick(MouseEvent event) throws IOException {
    	BaseHandler handler = new HomeHandle(getStage(), "/stationdetails/view/viewhome/HomeScreen.fxml");
    	switchTo(handler);
    }


    public void handleHelpClick(ActionEvent event) {
    }

    public void handleFeedbackClick(ActionEvent event) {
    }
    
    public void handleAdminClick(ActionEvent actionEvent) {
    	
    }

}
