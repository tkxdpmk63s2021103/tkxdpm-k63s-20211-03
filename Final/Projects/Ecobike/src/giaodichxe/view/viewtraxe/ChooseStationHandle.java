package giaodichxe.view.viewtraxe;

import giaodichxe.controller.DataController;
import giaodichxe.controller.ReturnBikeController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import giaodichxe.model.RentalBike;
import stationdetails.model.Station;
import view.BaseHandler;
import payment.controller.PaymentConfig;
import payment.view.PaymentHandler;
import payment.view.PaymentResultInterface;

import java.io.IOException;
import java.util.ArrayList;

public class ChooseStationHandle extends BaseHandler implements PaymentResultInterface{
    private RentalBike rentalBike;
    private Station station;
    private int money;
    private int status;

    @FXML
    TableView<Station> tableView;

    @FXML
    TableColumn<Station,String> name;

    @FXML
    TableColumn<Station,String> address;

    @FXML
    TableColumn<Station,Integer> singleBike;

    @FXML
    TableColumn<Station,Integer> eBike;

    @FXML
    TableColumn<Station,Integer> twinBike;

    @FXML
    TableColumn<Station,Integer> emptyDocks;


    private DataController dataController = new DataController();

    private ObservableList<Station> stationsOb;

    public ChooseStationHandle(String fxmlPath, RentalBike rentalBike) throws IOException {
        super(fxmlPath);
        setController(new ReturnBikeController());
        this.rentalBike = rentalBike;
        setListStation();
    }


    public void setListStation() {
        try {
            ArrayList<Station> stations = dataController.getListStation();
            stationsOb = FXCollections.observableArrayList(stations);

            name.setCellValueFactory(new PropertyValueFactory<Station,String>("stationName"));
            address.setCellValueFactory(new PropertyValueFactory<Station,String>("stationAddress"));
            singleBike.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfSingleBike"));
            eBike.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfEBike"));
            twinBike.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfTwinBike"));
            emptyDocks.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfEmptyDocks"));
            tableView.setItems(stationsOb);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void backToRentalBikeDetails(MouseEvent event) throws IOException {
        switchBack();
    }

    public void success(int cardId) {
        ReturnBikeController returnBikeController = new ReturnBikeController();
        returnBikeController.updateDataAfterReturnBike(rentalBike.getBike(),station.getStationId());
        BaseHandler handler = new ResultReturnBikeHandle("/giaodichxe/view/viewtraxe/ResultReturnBike.fxml");
        switchTo(handler);
    }

    public void handleStationSelected(MouseEvent event) throws IOException {
        ReturnBikeController controller = (ReturnBikeController) getController();
        money = controller.calculateRentalFees(rentalBike);
        if(money > 0){
            status = PaymentConfig.PAY;
        }else{
            status = PaymentConfig.REFUND;
            money = Math.abs(money);
        }
        station = tableView.getSelectionModel().getSelectedItem();
        BaseHandler handler = new PaymentHandler("/payment/view/fxml/PaymentScreen.fxml", "/payment/view/css/paymentStyle.css", status, money, "Giao dich tra tien xe bla bla");
        switchTo(handler);
    }

}
