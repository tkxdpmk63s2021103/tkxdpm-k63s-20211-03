package giaodichxe.view.viewtraxe;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import service.RentalBikeService;
import view.BaseHandler;
import giaodichxe.model.RentalBike;

import java.io.IOException;

public class EnterBarcodeHandle extends BaseHandler {

    @FXML
    private TextField barcodeTextField;

    public EnterBarcodeHandle(String fxmlPath) throws IOException {
        super(fxmlPath);
    }

    public void searchRentalBike(ActionEvent event) throws IOException {
        RentalBike rentalBike = RentalBikeService.getInstance().getRentalBikeFromBarcode(barcodeTextField.getText());
        if(rentalBike.getBike() != null){
            System.out.println("RentalBike tồn tại");
            BaseHandler handler = new RentalBikeDetailsHandle("/giaodichxe/view/viewtraxe/RentalBikeDetails.fxml",rentalBike);
            switchTo(handler);

        }else{
            showAlert("Barcode invalid");
        }
    }

    private void showAlert(String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Admin Login");
        alert.setContentText(text);
        alert.showAndWait();
    }

    public void backToHome(ActionEvent event) throws IOException {
       switchBack();
    }

}
