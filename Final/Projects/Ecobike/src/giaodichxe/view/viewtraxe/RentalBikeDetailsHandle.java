package giaodichxe.view.viewtraxe;

import giaodichxe.controller.ReturnBikeController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import view.BaseHandler;
import giaodichxe.model.RentalBike;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
public class RentalBikeDetailsHandle extends BaseHandler {
    @FXML
    Label Barcode;

    @FXML
    Label Name;

    @FXML
    Label Type;

    @FXML
    Label Weight;

    @FXML
    Label LicensePlate;

    @FXML
    Label ManufacturingDate;

    @FXML
    Label Producer;

    @FXML
    Label textPay;

    @FXML
    Label rentBikeDay;

    @FXML
    Label moneyToPay;

    @FXML
    ImageView ViewBike;

    private RentalBike rentalBike;

    private ReturnBikeController returnBikeController = new ReturnBikeController();


    public RentalBikeDetailsHandle(String fxmlPath, RentalBike rentalBike) throws IOException {
        super(fxmlPath);
        this.rentalBike = rentalBike;
        setInfoRentalBike(rentalBike);
    }

    public void setInfoRentalBike(RentalBike rentalBike) throws MalformedURLException {
        Barcode.setText(rentalBike.getBike().getBarcode());
        Name.setText(rentalBike.getBike().getName());
        Type.setText(rentalBike.getBike().getType());
        Weight.setText(String.valueOf(rentalBike.getBike().getWeight()));
        LicensePlate.setText(rentalBike.getBike().getLicensePlate());
        ManufacturingDate.setText(String.valueOf(rentalBike.getBike().getManufacturingDate()));
        Producer.setText(rentalBike.getBike().getProducer());
        rentBikeDay.setText(rentalBike.getStartTime().toString());
        ReturnBikeController returnBikeController = new ReturnBikeController();
        int moneyPay = returnBikeController.calculateRentalFees(rentalBike);
        if(moneyPay > 0){
            textPay.setText("Cần thanh toán");
        }else{
            textPay.setText("Hoàn tiền");
        }
        moneyToPay.setText(Math.abs((returnBikeController.calculateRentalFees(rentalBike)))+"VND");
        File file = new File(rentalBike.getBike().getImgPath());
        Image image = new Image(file.toURI().toURL().toString());
        ViewBike.setImage(image);
    }


    public void confirmReturnBike(ActionEvent event) throws IOException {
        BaseHandler handler = new ChooseStationHandle("/giaodichxe/view/viewtraxe/ChooseStation.fxml",rentalBike);
        switchTo(handler);
    }

    public void backToEnterBarcode(ActionEvent event) throws IOException {
        switchBack();
    }
}
