package giaodichxe.controller;

import controller.BaseController;
import giaodichxe.model.Bike;
import giaodichxe.CalculatorLogic.FeesCalculator01;
import giaodichxe.CalculatorLogic.FeesCalculatorInterface;
import giaodichxe.model.RentalBike;
import service.BikeService;
import service.RentalBikeService;
import service.StationService;


public class ReturnBikeController extends BaseController {


    public int calculateRentalFees(RentalBike rentalBike) {
        FeesCalculatorInterface feesCalculatorInterface = new FeesCalculator01();
        return feesCalculatorInterface.calculateRentalFees(rentalBike);
    }


    public void updateDataAfterReturnBike(Bike bike, int stationId) {
        System.out.println("Call update return bike function");
        BikeService.getInstance().lockBike(bike.getBarcode(),stationId);
        StationService.getInstance().updateStationAfterReturnBike(stationId,bike.getType());
        RentalBikeService.getInstance().deleteRentalBike(bike);
    }
}
