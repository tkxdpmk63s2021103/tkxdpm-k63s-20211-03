package giaodichxe.controller;

import controller.BaseController;
import giaodichxe.model.Bike;
import service.BikeService;
import service.StationService;
import stationdetails.model.Station;

import java.util.ArrayList;

public class DataController extends BaseController {

    public  ArrayList<Station> getListStation(){
        return StationService.getInstance().getListStation();
    }

    public ArrayList<Station> searchStation(String input){
        return StationService.getInstance().searchStation((input));
    }

    public ArrayList<Bike> getBikeFromStation(int stationId) {
        return BikeService.getInstance().getBikeFromStation(stationId);
    }

    public Bike getBikeFromBarcode(String barcode){
        return BikeService.getInstance().getBikeFromBarcode(barcode);
    }
   
}
