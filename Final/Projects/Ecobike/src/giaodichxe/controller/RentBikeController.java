package giaodichxe.controller;


import controller.BaseController;
import giaodichxe.CalculatorLogic.FeesCalculator01;
import giaodichxe.CalculatorLogic.FeesCalculatorInterface;
import giaodichxe.model.Bike;
import service.BikeService;
import service.RentalBikeService;
import service.StationService;

public class RentBikeController extends BaseController {

    public int calculateDeposit(String bikeType){
        FeesCalculatorInterface feesCalculatorInterface = new FeesCalculator01();
        return feesCalculatorInterface.getDeposit(bikeType);
    }

    public void updateDataAfterRentBike(Bike bike,int cardId){
        System.out.println("Call update function on rent bike");
        BikeService.getInstance().unlockBike(bike.getBarcode());
        StationService.getInstance().updateStationAfterRentBike(bike.getStationId(),bike.getType());
        RentalBikeService.getInstance().saveRentalBike(bike,cardId);
    }
}
