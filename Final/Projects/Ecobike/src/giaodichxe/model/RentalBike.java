package giaodichxe.model;


import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import giaodichxe.controller.DataController;
import controller.Main;

import java.sql.*;
import java.util.Date;

public class RentalBike {
    private Bike bike;
    private int cardId;
    private Date startTime;
    private Date endTime;
    public static SQLServerDataSource ds = new Main().getConnect();
//    public static DataController dataController = new DataController();

    public RentalBike() {
    }

    public Bike getBike() {
        return bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
