package giaodichxe.CalculatorLogic;

import giaodichxe.model.RentalBike;

public interface FeesCalculatorInterface {

	public int calculateRentalFees(RentalBike rentalBike);
	public int getDeposit(String bikeType);
}
