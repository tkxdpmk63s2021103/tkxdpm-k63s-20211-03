package giaodichxe.CalculatorLogic;


import giaodichxe.model.RentalBike;

import java.util.Date;

public class FeesCalculator01 implements FeesCalculatorInterface {


	@Override
	public int calculateRentalFees(RentalBike rentalBike) {
		int phiThueXe = 0;
		Date date = new Date();
		System.out.println(date.getDay()+" "+date.getHours()+" "+date.getMinutes());
		System.out.println(rentalBike.getStartTime().getDay()+" "+rentalBike.getStartTime().getHours()+" "+rentalBike.getStartTime().getMinutes());
		long hieu = (date.getTime() - rentalBike.getStartTime().getTime())/1000;
		int phut = (int) hieu/60;
		System.out.println("So phut : "+phut);
		if(phut <= 10){
			System.out.println("Không cần thanh toán");
		}else{
			if(phut<=30){
				phiThueXe = 10000;

			}else{
				phiThueXe = ((phut-30)/15)*3000 + 10000;
			}
		}
		int tienCoc = 0;
		if(rentalBike.getBike().getType().equals("SingleBike")){
			tienCoc = 400000;
		}else if(rentalBike.getBike().getType().equals("EBike")){
			tienCoc = 700000;
			phiThueXe = phiThueXe * 3/2;
		}else if(rentalBike.getBike().getType().equals("TwinBike")){
			tienCoc = 550000;
			phiThueXe = phiThueXe * 3/2;
		}

		return phiThueXe - tienCoc;
	}

	@Override
	public int getDeposit(String bikeType) {
		int deposit = -1;
		if(bikeType.equals("SingleBike")){
			deposit = 400000;
		} else if(bikeType.equals("EBike")){
			deposit =  700000;
		} else if(bikeType.equals("TwinBike")){
			deposit =  550000;
		}
		return deposit;
	}
}
