package stationdetails.view.viewhome;

import giaodichxe.controller.DataController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import stationdetails.model.Station;
import stationdetails.view.viewstaiondetails.StationDetailsHandle;
import view.BaseHandler;
import management.admin.AdminHandle;
import management.bikemanagement.BikeManagementHandler;
import giaodichxe.view.viewtraxe.EnterBarcodeHandle;

import java.io.IOException;
import java.util.ArrayList;

public class HomeHandle extends BaseHandler{


    @FXML
    TableView<Station> tableView;

    @FXML
    TextField searchInput;

    @FXML
    TableColumn<Station,String> name;

    @FXML
    TableColumn<Station,String> address;

    @FXML
    TableColumn<Station,Integer> singleBike;

    @FXML
    TableColumn<Station,Integer> eBike;

    @FXML
    TableColumn<Station,Integer> twinBike;

    private ObservableList<Station> stationsOb;

    DataController dataController;

    public HomeHandle(Stage stage, String fxmlPath) throws IOException {
        super(stage, fxmlPath);
        setController(new DataController());
        dataController = (DataController) getController();
        init();
    }


    public void handleHomeClick(ActionEvent actionEvent) {
    }

    public void handleReturnBikeClick(ActionEvent actionEvent) throws IOException {
        BaseHandler handler = new EnterBarcodeHandle("/giaodichxe/view/viewtraxe/EnterBarcode.fxml");
        switchTo(handler);
    }

    public void handleHelpClick(ActionEvent actionEvent) {
    }

    public void handleFeedbackClick(ActionEvent actionEvent) {
    }
    
    public void handleAdminClick(ActionEvent actionEvent) throws IOException {
        BaseHandler handler = new AdminHandle("/management/admin/AdminView.fxml");
        switchTo(handler);
    }
    
    public void handleBikeManageClick(ActionEvent actionEvent) throws IOException {
        if (AdminHandle.hasLoginAsAdmin()) {
        	BaseHandler handler = new BikeManagementHandler("/management/bikemanagement/BikeManagement.fxml");
            switchTo(handler);
        } else {
        	this.showAlert("INFORMATION", "Can not access without logged in as admin!");
        }
    }

    public void handleSearchEnter(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)) {
            String input = searchInput.getText();

            if (!input.trim().isEmpty()) {
                ArrayList<Station> listNewStation = dataController.searchStation(input);
                ObservableList<Station> stationsOb = FXCollections.observableArrayList(listNewStation);
                tableView.setItems(stationsOb);
                tableView.refresh();
            }
        }
    }

    public void handleStationSelected(MouseEvent mouseEvent) {

    }


    public void init() {

        try {
            ArrayList<Station> stations = dataController.getListStation();
            stationsOb = FXCollections.observableArrayList(stations);

            name.setCellValueFactory(new PropertyValueFactory<Station,String>("stationName"));
            address.setCellValueFactory(new PropertyValueFactory<Station,String>("stationAddress"));
            singleBike.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfSingleBike"));
            eBike.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfEBike"));
            twinBike.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfTwinBike"));

            tableView.setItems(stationsOb);
        } catch (Exception e) {
          e.printStackTrace();
        }

    }

    public void showStationDetails(MouseEvent mouseEvent) throws IOException {
        BaseHandler handler = new StationDetailsHandle("/stationdetails/view/viewstaiondetails/StationDetails.fxml",tableView.getSelectionModel().getSelectedItem());
        switchTo(handler);
    }

    public void back(ActionEvent event) throws IOException {
    }
}
