package stationdetails.view.viewstaiondetails;

import giaodichxe.controller.DataController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import giaodichxe.model.Bike;
import stationdetails.model.Station;
import view.BaseHandler;
import giaodichxe.view.viewmuonxe.BikeDetailsHandle;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

public class StationDetailsHandle extends BaseHandler {
    private Station station;
    @FXML
    Label stationId;

    @FXML
    Label name;

    @FXML
    Label address;

    @FXML
    Label numberOfSingleBike;

    @FXML
    Label numberOfTwinBike;

    @FXML
    Label numberOfEBike;

    @FXML
    Label numberOfEmpty;

    @FXML
    private TableView<Bike> tableView;

    @FXML
    TableColumn<Bike, String> barcode;

    @FXML
    TableColumn<Bike,String> bikeName;

    @FXML
    TableColumn<Bike,String> type;

    @FXML
    TableColumn<Bike,String> licensePlate;

    @FXML
    TableColumn<Bike,String> producer;

    @FXML
    ImageView imgStation;

    private ObservableList<Bike> bikesOb;
    private DataController dataController = new DataController();

    public StationDetailsHandle(String fxmlPath, Station station) throws IOException {
        super(fxmlPath);
        this.station = station;
        setData(station);
    }


    public void  setData(Station station){
        stationId.setText(String.valueOf(station.getStationId()));
        name.setText(station.getStationName());
        address.setText(station.getStationAddress());
        numberOfSingleBike.setText(String.valueOf(station.getNumberOfSingleBike()));
        numberOfTwinBike.setText(String.valueOf(station.getNumberOfTwinBike()));
        numberOfEBike.setText(String.valueOf(station.getNumberOfEBike()));
        numberOfEmpty.setText(String.valueOf(station.getNumberOfEmptyDocks()));
        File file = new File("C:\\Users\\ducma\\IdeaProjects\\Ecobike\\src\\Img\\station.jpg");
        Image image = null;
        try {
            image = new Image(file.toURI().toURL().toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        imgStation.setImage(image);

        try {
            ArrayList<Bike> bikes = dataController.getBikeFromStation(station.getStationId());
            bikesOb = FXCollections.observableArrayList(bikes);

            barcode.setCellValueFactory(new PropertyValueFactory<Bike,String>("barcode"));
            bikeName.setCellValueFactory(new PropertyValueFactory<Bike,String>("name"));
            type.setCellValueFactory(new PropertyValueFactory<Bike,String>("type"));
            licensePlate.setCellValueFactory(new PropertyValueFactory<Bike,String>("licensePlate"));
            producer.setCellValueFactory(new PropertyValueFactory<Bike,String>("producer"));



            tableView.setItems(bikesOb);
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Danh sách rỗng.");
            alert.show();
        }
    }

    public void selectBike(MouseEvent mouseEvent) throws IOException {
        BaseHandler handler = new BikeDetailsHandle("/giaodichxe/view/viewmuonxe/BikeDetails.fxml",tableView.getSelectionModel().getSelectedItem());
        switchTo(handler);
    }

    public void back(ActionEvent event) throws IOException {
        switchBack();
    }
}
