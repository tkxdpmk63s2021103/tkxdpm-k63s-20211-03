package payment.controller;

/**
 * Lớp điều thanh toán gốc
 */
import payment.exception.PaymentException;
import payment.model.Card;
import payment.model.Transaction;

public abstract class BasePayment {
	public abstract BasePayment create();

	public abstract Transaction request(Card card, int amount, String message) throws PaymentException;
}
