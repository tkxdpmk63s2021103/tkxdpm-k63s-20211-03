package payment.controller;

/**
 * Lớp thực hiện chuẩn bị dữ liệu để thanh toán và lưu giao dịch nếu thanh toán thành công
 */
import controller.BaseController;
import payment.exception.PaymentException;
import payment.model.Card;
import payment.model.Transaction;

public class PaymentController extends BaseController {
	
	private Card card;
	
	/**
	 * 
	 * @param payType loại thanh toán
	 * @param amount lương tiền
	 * @param message tin nhắn giao dịch
	 * @throws PaymentException
	 */
	public void request(int payType, int amount, String message) throws PaymentException {
		BasePayment controller = PaymentFactory.create(payType);
		Transaction transaction = controller.request(card, amount, message);
		transaction.save();
	}
	
	/**
	 * Gán thông tin thẻ 
	 * @param card
	 */
	public void setCard(Card card) {
		this.card = card;
	}
	
}

