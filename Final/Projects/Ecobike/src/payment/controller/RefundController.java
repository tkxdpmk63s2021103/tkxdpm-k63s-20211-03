package payment.controller;
/**
 * Lớp thực hiện chức năng hoàn tiền
 */
import payment.exception.PaymentException;
import payment.model.Card;
import payment.model.Transaction;
import payment.subsystem.RefundInterbankInterface;
import payment.subsystem.RefundInterbankSubsystem;

public class RefundController extends BasePayment{

	static {
		PaymentFactory.register(PaymentConfig.REFUND, new RefundController());
	}
	
	private RefundInterbankInterface interbank;
	
	public RefundController() {
		interbank = new RefundInterbankSubsystem();
	}
	
	public BasePayment create() {
		return new RefundController();
	}
	
	/**
	 * @param card: thông tin thẻ thanh toán
	 * @param amount: lượng tiền thanh toán
	 * @param messsage: tin nhắn giao dịch 
	 * @return thông tin giao dịch
	 */
	public Transaction request(Card card, int amount, String message) throws PaymentException {
		return interbank.requestToRefund(card, amount, message);
	}

}
