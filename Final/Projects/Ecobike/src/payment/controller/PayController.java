package payment.controller;

/**
 * Lớp thực thi chức năng trừ tiền
 */
import payment.exception.PaymentException;
import payment.model.Card;
import payment.model.Transaction;
import payment.subsystem.PayInterbankInterface;
import payment.subsystem.PayInterbankSubsystem;

public class PayController extends BasePayment {

	static {
		PaymentFactory.register(PaymentConfig.PAY, new PayController());
	}
	
	private PayInterbankInterface interbank;
	
	public PayController() {
		interbank = new PayInterbankSubsystem();
	}
	
	public BasePayment create() {
		return new PayController();
	}
	
	/**
	 * @param card: thông tin thẻ thanh toán
	 * @param amount: lượng tiền thanh toán
	 * @param messsage: tin nhắn giao dịch 
	 * @return thông tin giao dịch
	 */
	public Transaction request(Card card, int amount, String message) throws PaymentException {
		return interbank.requestToPay(card, amount, message);
	}

}
