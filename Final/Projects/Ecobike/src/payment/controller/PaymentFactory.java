package payment.controller;

import java.util.HashMap;

public class PaymentFactory {
	private static HashMap<Integer, BasePayment> payments = new HashMap<Integer, BasePayment>();
	
	public static void register(int payType, BasePayment payment) {
		payments.put(payType, payment);
	}
	
	public static BasePayment create(int payType) {
		return ((BasePayment)payments.get(payType)).create();
	}
}
