package payment.view;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import payment.controller.PaymentController;
import payment.exception.CreditCardException;
import payment.exception.PaymentException;
import payment.model.Bank;
import payment.model.CreditCard;
import view.BaseHandler;

/**
 * Lớp điều khiển màn hình thanh toán
 * 
 * @author Long Phan
 *
 */
public class PaymentHandler extends BaseHandler {
	
	private int payType;
	private int amount;
	private String message;
	private HashMap<String, Integer> bankMap;

	static {
		try {
			Class.forName("payment.controller.PayController");
			Class.forName("payment.controller.RefundController");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private ChoiceBox<String> bankChoiceBox;
	
	@FXML
	private TextField cardNumber;
	
	@FXML
	private TextField cardHolder;
	
	@FXML
	private TextField expirationDate;
	
	@FXML
	private TextField cvc;
	
	@FXML
	private Button confirmBtn;
	
	@FXML
	private Button cancelBtn;
	
	private void init() {
		bankMap = new HashMap<String, Integer>();
		setController(new PaymentController());
		
		Iterator<Bank> banks = Bank.getAll();
		while(banks.hasNext()) {
			Bank bank = banks.next();
			bankMap.put(bank.getName(), bank.getId());
			bankChoiceBox.getItems().add(bank.getName());
		}
		
		confirmBtn.setOnMouseClicked(e -> confirm(e));
		confirmBtn.setOnMouseEntered(e -> buttonMouseEnter(e));
		confirmBtn.setOnMouseExited(e -> buttonmMouseExit(e));
		
		cancelBtn.setOnMouseClicked(e -> cancel(e));
		cancelBtn.setOnMouseEntered(e -> buttonMouseEnter(e));
		cancelBtn.setOnMouseExited(e -> buttonmMouseExit(e));
	}
	
	public PaymentHandler(String fxmlPath, int payType, int amount, String message) throws IOException {
		super(fxmlPath);
		this.payType = payType;
		this.amount = amount;
		this.message = message;
		init();
	
	}
	
	public PaymentHandler(String fxmlPath, String cssPath, int payType, int amount, String message) throws IOException {
		super(fxmlPath, cssPath);
		this.payType = payType;
		this.amount = amount;
		this.message = message;
		init();
	}
	
	/**
	 * Hàm thực hiển thanh toán, thanh toán thành công sẽ chuyển màn hình kết quả và ngược lại nếu thất bại
	 * @param e
	 */
	public void confirm(MouseEvent e) {
		PaymentController controller = (PaymentController)getController();
		try {
			CreditCard card = new CreditCard();
			card.setCardNumber(cardNumber.getText());
			card.setCardHolderName(cardHolder.getText());
			card.setExpirationDate(expirationDate.getText());
			card.setSecurityCode(cvc.getText());
			card.setBankId(bankMap.get(bankChoiceBox.getValue()));
			
			controller.setCard(card);
			controller.request(payType, amount, message);
			
			PaymentResultInterface result = (PaymentResultInterface)getParent();
			result.success(Integer.parseInt(cardNumber.getText()));
			
			
		} catch (PaymentException | CreditCardException error) {
			BaseHandler errorScreen = new PaymentErrorHandler("/payment/view/fxml/PaymentErrorScreen.fxml", error.getMessage());
			popUp(errorScreen);

		}
		
	}
	
	/**
	 * Quay lại màn hình cũ
	 * @param e
	 */
	public void cancel(MouseEvent e) {
		switchBack();
	}
	
	public void buttonMouseEnter(MouseEvent e) {
		Button btn = (Button)e.getSource();
		btn.setStyle("-fx-background-color: #f2f2f0; -fx-border-width: 2px");
	}
	
	public void buttonmMouseExit(MouseEvent e) {
		Button btn = (Button)e.getSource();
		btn.setStyle("-fx-background-color: #ffffff; -fx-border-width: 2px");
	}
}
