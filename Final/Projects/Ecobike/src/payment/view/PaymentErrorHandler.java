package payment.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import view.BaseHandler;

/**
 * Lớp điều khiển màn hình thanh toán lỗi
 * 
 * @author Long Phan
 *
 */
public class PaymentErrorHandler extends BaseHandler {
	
	@FXML
	private Label message;
	
	@FXML 
	private Button closeBtn;
	
	public PaymentErrorHandler(String fxmlPath, String message) {
		super(fxmlPath);
		
		this.message.setText(message);
		closeBtn.setOnMouseClicked(e -> close(e));
	}
	
	public void close(MouseEvent e) {
		super.close();
	}
	
}
