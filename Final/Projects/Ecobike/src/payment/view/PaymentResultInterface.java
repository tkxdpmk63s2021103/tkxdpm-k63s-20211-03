package payment.view;

/**
 * Cài đặt trả về kết quả thanh toán
 * 
 * @author Long Phan
 *
 */
public interface PaymentResultInterface {
	public void success(int cardId);
}
