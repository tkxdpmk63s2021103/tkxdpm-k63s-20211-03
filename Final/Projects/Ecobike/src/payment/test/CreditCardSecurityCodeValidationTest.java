package payment.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import payment.validation.CreditCardSecurityCodeValidation;

class CreditCardSecurityCodeValidationTest {

	@ParameterizedTest
	@CsvSource({
		", false",
		"123, true",
		"543, true",
		"asd, false",
		"!#2m, false"
	})


	void test(String code, boolean expected) {
		boolean isValid = CreditCardSecurityCodeValidation.validate(code);
		assertEquals(isValid, expected);
	}

}
