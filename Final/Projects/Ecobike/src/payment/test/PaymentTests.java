package payment.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({CreditCardNumberValidationTest.class,  CreditCardHolderValidationTest.class, CreditCardDateValidationTest.class, CreditCardSecurityCodeValidationTest.class})
public class PaymentTests {

}
