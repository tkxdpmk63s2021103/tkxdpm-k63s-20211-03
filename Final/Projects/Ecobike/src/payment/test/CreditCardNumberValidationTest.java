package payment.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import payment.validation.CreditCardNumberValidation;

class CreditCardNumberValidationTest {

	@ParameterizedTest
	@CsvSource({
		", false",
		"a, false",
		"@, false",
		"1, false",
		"12345678, true",
		"1234567a, false",
		"123456 a, false"
	})

	void test(String number, boolean expected) {
		boolean isValid = CreditCardNumberValidation.validate(number);
		assertEquals(isValid, expected);
	}

}
