package payment.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import payment.validation.CreditCardDateValidation;

class CreditCardDateValidationTest {

	@ParameterizedTest
	@CsvSource({
		", false",
		"@#!@, false",
		"13/22, false",
		"02/12, false",
		"02/22, true",
		"01/22, false",
		"12, 21, false"
	})


	void test(String date, boolean expected) {
		boolean isValid = CreditCardDateValidation.validate(date);
		assertEquals(isValid, expected);
	}

}
