package payment.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import payment.validation.CreditCardHolderValidation;
import payment.validation.CreditCardNumberValidation;

class CreditCardHolderValidationTest {

	@ParameterizedTest
	@CsvSource({
		", false",
		"1, false",
		"a, true",
		"wqe eqw we, true",
		"@ ww eqw, false",
		"* *%%$$, false",
		"123h312k3h1 312, false"
	})

	void test(String name, boolean expected) {
		boolean isValid = CreditCardHolderValidation.validate(name);
		assertEquals(isValid, expected);
	}

}
