package payment.validation;

public class CreditCardSecurityCodeValidation {
	public static boolean validate(String code) {
		if(code == null) return false;
		
		try {
			int code_int = Integer.parseInt(code);			
			if(code_int < 100 || code_int > 999) return false;
			return true;
		}
		catch(NumberFormatException e) {
			return false;
		}
	
	}
}
