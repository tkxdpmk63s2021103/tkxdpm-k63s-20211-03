package payment.validation;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import payment.exception.PaymentException;

public class CreditCardDateValidation{
	
	public static boolean validate(String expirationDate) {
		if(expirationDate == null) return false;
		Pattern pattern = Pattern.compile("^[0-9]{2}/[0-9]{2}$");
    	Matcher matcher = pattern.matcher(expirationDate);
    	if(!matcher.find()) {
    		return false;
    	}
		
		Date now = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		
		String[] date = expirationDate.split("/");
		
		if(date[0].length() != 2 || date[1].length() != 2 || Integer.parseInt(date[0]) > 11) {
			return false;
		}
		else {
			if (Integer.parseInt(date[1]) < year % 2000) {
				return false;
			}
			else if(Integer.parseInt(date[1]) == year % 2000) {
				if(Integer.parseInt(date[0]) <= month + 1) {
					return false;
				}
			}
		}
		
		return true;
		
	}
}
