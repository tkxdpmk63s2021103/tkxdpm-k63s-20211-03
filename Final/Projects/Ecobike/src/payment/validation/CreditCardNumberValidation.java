package payment.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreditCardNumberValidation {
	public static boolean validate(String number) {
		if(number == null) return false;
		Pattern pattern = Pattern.compile("[0-9]{8}");
    	Matcher matcher = pattern.matcher(number);
    	return matcher.find();
	}
}
