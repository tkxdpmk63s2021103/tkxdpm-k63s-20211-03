package payment.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreditCardHolderValidation {
	public static boolean validate(String name) {
		if (name == null) return false;
		Pattern pattern = Pattern.compile("^[a-zA-Z ]+$");
    	Matcher matcher = pattern.matcher(name);
    	return matcher.find();
	}
}
