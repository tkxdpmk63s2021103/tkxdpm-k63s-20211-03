package payment.model;
import payment.exception.CreditCardException;
import payment.exception.PaymentException;
import payment.validation.CreditCardDateValidation;
import payment.validation.CreditCardHolderValidation;
import payment.validation.CreditCardNumberValidation;
import payment.validation.CreditCardSecurityCodeValidation;

/**
 * Quản lý thông tin thẻ tín dụng
 * @author Long Phan
 *
 */
public class CreditCard extends Card{
	private String cardHolderName;
    private String cardNumber;
    private int bankId;
    private String expirationDate;
    private int securityCode;

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) throws CreditCardException {
    	if(!CreditCardHolderValidation.validate(cardHolderName)) {
    		throw new CreditCardException("Card holder is not valid");
    	}
        this.cardHolderName = cardHolderName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) throws CreditCardException {
    	if(!CreditCardNumberValidation.validate(cardNumber)) {
    		throw new CreditCardException("Card number is not valid");
    	}
    	this.cardNumber = cardNumber;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) throws CreditCardException {
    	if(!CreditCardDateValidation.validate(expirationDate)) {
    		throw new CreditCardException("Expiration date is not valid");
    	}
    	this.expirationDate = expirationDate;
    }

    public int getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) throws CreditCardException {
    	if(!CreditCardSecurityCodeValidation.validate(securityCode)) {
    		throw new CreditCardException("Security code is not valid");
    	}
        this.securityCode = Integer.parseInt(securityCode);
    }

	    
}
