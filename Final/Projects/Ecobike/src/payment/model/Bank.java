package payment.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;

import controller.Main;

/**
 * Quản lý thông tin ngân hàng
 * @author Long Phan
 *
 */
public class Bank {
	private int id;
	private String name;
	
	private static SQLServerDataSource ds = new Main().getConnect();
	
	public Bank() {
		
	}
	
	public static Iterator<Bank> getAll() {
		ArrayList<Bank> banks = new ArrayList<>();
		Connection cnn;
		try {
			cnn = ds.getConnection();
			Statement statement = cnn.createStatement();
			ResultSet resultSet = statement.executeQuery("select id, name from bank");
			while(resultSet.next()) {
				Bank bank = new Bank();
				bank.setId(resultSet.getInt(1));
				bank.setName(resultSet.getString(2));
				banks.add(bank);
			}
		} catch (SQLServerException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return banks.iterator();
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	
	
	
}
