package payment.subsystem.interbank.view;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import controller.Main;
import payment.exception.PaymentException;
import payment.model.CreditCard;
import payment.model.CreditCardTransaction;


public class InterbankView {
    private SQLServerDataSource ds = new Main().getConnect();
    
    /**
     * Gọi API giả định để thanh toán
     * @param card thông tin thẻ tín dụng
     * @param amount lượng tiền
     * @param message tin nhắn giao dịch
     * @param action loại hành động
     * @return thông tin giao dịch của thẻ tín dụng
     * @throws PaymentException
     */
	public CreditCardTransaction post(CreditCard card, int amount, String message, int action) throws PaymentException {
		
		try {
			Connection conn = ds.getConnection();
			PreparedStatement statement  = conn.prepareStatement(
					"select money from card "
					+ "where cardNumber = ? "
					+ "and cardholderName = ? "
					+ "and expirationDate = ? "
					+ "and securityCode = ? "
					+ "and bankId = ? ");
			
			statement.setString(1, card.getCardNumber());
			statement.setString(2, card.getCardHolderName());
			statement.setString(3, card.getExpirationDate());
			statement.setInt(4, card.getSecurityCode());
			statement.setInt(5, card.getBankId());
			
			
			ResultSet result = statement.executeQuery();
			int money = -1;
			
			while(result.next()) {
				money = result.getInt(1);
			}
			
			if (money == -1) {
				throw new PaymentException("Invalid credit card");
			}
			
			if (action == -1) {
				if(money < amount) {
					throw new PaymentException("Not enough money");
				}			
			}
			money = money + action * amount;
			
			statement  = conn.prepareStatement("update card set money = ? where cardNumber = ?");
			statement.setInt(1, money);
			statement.setString(2, card.getCardNumber());
			statement.executeUpdate();
			
			Random rand = new Random();
			Date sqlDate = new Date(System.currentTimeMillis());
			CreditCardTransaction transaction = new CreditCardTransaction(rand.nextInt(1000000), sqlDate, amount, message,
					card.getCardNumber(), card.getCardHolderName());
			
			return transaction;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
		
	}
}
