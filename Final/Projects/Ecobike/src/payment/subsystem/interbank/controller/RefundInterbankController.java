package payment.subsystem.interbank.controller;

import payment.exception.PaymentException;
import payment.model.CreditCard;
import payment.model.CreditCardTransaction;
import payment.subsystem.interbank.view.InterbankView;

public class RefundInterbankController {
	private InterbankView view;
	
	public RefundInterbankController() {
		view = new InterbankView();
	}
	
	public CreditCardTransaction refund(CreditCard card, int amount, String message) throws PaymentException {
		CreditCardTransaction transaction = view.post(card, amount, message, 1);
		
		return transaction;
	}
}
