package payment.subsystem.interbank.controller;

import payment.exception.PaymentException;
import payment.model.CreditCard;
import payment.model.CreditCardTransaction;
import payment.subsystem.interbank.view.InterbankView;

public class PayInterbankController {
	private InterbankView view;
	
	public PayInterbankController() {
		view = new InterbankView();
	}
	
	public CreditCardTransaction pay(CreditCard card, int amount, String message) throws PaymentException {
		CreditCardTransaction transaction = view.post(card, amount, message, -1);
		return transaction;
	}
}
