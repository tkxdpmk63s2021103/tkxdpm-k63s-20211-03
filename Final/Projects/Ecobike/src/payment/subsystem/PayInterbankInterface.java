package payment.subsystem;

import payment.exception.PaymentException;
import payment.model.Card;
import payment.model.Transaction;

public interface PayInterbankInterface {
	public Transaction requestToPay(Card card, int amount, String message) throws PaymentException;
}
