package payment.subsystem;

import payment.exception.PaymentException;
import payment.model.Card;
import payment.model.CreditCard;
import payment.model.CreditCardTransaction;
import payment.model.Transaction;
import payment.subsystem.interbank.controller.PayInterbankController;

public class PayInterbankSubsystem implements PayInterbankInterface {
	private PayInterbankController controller;
	
	public PayInterbankSubsystem() {
		controller = new PayInterbankController();
	}
	
	public Transaction requestToPay(Card card, int amount, String message) throws PaymentException {
		CreditCardTransaction transaction = controller.pay((CreditCard)card, amount, message);
		return transaction;
	}
}
