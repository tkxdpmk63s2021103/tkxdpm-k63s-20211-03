package payment.subsystem;

import payment.exception.PaymentException;
import payment.model.Card;
import payment.model.CreditCard;
import payment.model.CreditCardTransaction;
import payment.model.Transaction;
import payment.subsystem.interbank.controller.RefundInterbankController;

public class RefundInterbankSubsystem implements RefundInterbankInterface {

	private RefundInterbankController controller;
	
	public RefundInterbankSubsystem () {
		controller = new RefundInterbankController();
	}
	
	public Transaction requestToRefund(Card card, int amount, String message) throws PaymentException {
		CreditCardTransaction transaction = controller.refund((CreditCard)card, amount, message);
		return transaction;
	}

}
