module Ecobike {
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.base;
    requires com.microsoft.sqlserver.jdbc;
    requires java.sql;
    requires java.naming;
//	requires org.junit.jupiter.api;
	requires junit;
	requires org.junit.jupiter.api;
	requires org.junit.jupiter.params;
//
//	requires org.junit.jupiter.params;
//    requires junit;
//    requires org.testng;


    opens controller;

    opens giaodichxe.view.viewtraxe;
    opens giaodichxe.view.viewmuonxe;
    opens giaodichxe.model;
    opens giaodichxe.controller;
    opens giaodichxe.CalculatorLogic;

    opens CSS;


    opens stationdetails.view.viewhome;
    opens stationdetails.view.viewstaiondetails;
    opens stationdetails.model;

    opens management.bikemanagement;
    opens management.bikemanagement.bikeadd;
    opens management.bikemanagement.bikeeditor;
    opens management.admin;
    opens service;
    opens repository;
    
    opens payment.view;
}