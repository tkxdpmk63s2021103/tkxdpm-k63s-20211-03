package view;

import java.io.IOException;

import controller.BaseController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

public class BaseHandler {
	private Stage stage;
	private Scene scene;
	private BaseHandler parent;
	private BaseController controller;
	
	public BaseHandler(String fxmlPath) {
		load(fxmlPath);
	}
	
	public BaseHandler(String fxmlPath, String cssPath){
		load(fxmlPath);
		this.scene.getStylesheets().add(getClass().getResource(cssPath).toExternalForm());
	}
	
	public BaseHandler(Stage stage, String fxmlPath){
		load(fxmlPath);
		this.stage = stage;
	}
	
	public BaseHandler(Stage stage, String fxmlPath, String cssPath) {
		this(fxmlPath, cssPath);
		this.stage = stage;
	}
	
	private void load(String fxmlPath){
		FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlPath));
		loader.setController(this);
		try {
			Parent root = loader.load();
			this.scene = new Scene(root);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void switchTo(BaseHandler handler) {
		handler.setStage(this.stage);
		handler.setParent(this);
		handler.show();
	}
	
	public void switchBack() {
		try {
			this.parent.show();			
		} catch(NullPointerException e) {
			e.printStackTrace();
		}
	}
	
	public void popUp(BaseHandler handler) {
		Stage stage = new Stage();
		handler.setStage(stage);
		handler.setParent(this);
		handler.show();
	}
	
	public void close() {
		this.stage.close();
	}
	
	public void show() {
		this.stage.setScene(this.scene);
		this.stage.show();
	}
	
	protected void showAlert(String header, String message) {
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setHeaderText(header);
		alert.setContentText(message);
		alert.show();
	}
	
	
	public void setController(BaseController controller) {
		this.controller = controller;
	}
	
	public BaseController getController() {
		return this.controller;
	}
	
	public Stage getStage() {
		return this.stage;
	}
	
	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
	public void setParent(BaseHandler parent) {
		this.parent = parent;
	}
	
	public BaseHandler getParent() {
		return this.parent;
	}
}
