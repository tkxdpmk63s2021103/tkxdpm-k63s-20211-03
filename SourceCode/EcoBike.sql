USE [Ecobike]
GO
/****** Object:  User [ducmanh]    Script Date: 1/4/2022 4:03:11 PM ******/
CREATE USER [ducmanh] FOR LOGIN [ducmanh] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[bank]    Script Date: 1/4/2022 4:03:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bank](
	[name] [nvarchar](50) NULL,
	[id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bike]    Script Date: 1/4/2022 4:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bike](
	[barcode] [varchar](20) NOT NULL,
	[name] [nvarchar](20) NOT NULL,
	[type] [varchar](10) NOT NULL,
	[weight] [int] NOT NULL,
	[licensePlate] [varchar](20) NOT NULL,
	[manuafacturingDate] [date] NOT NULL,
	[producer] [nvarchar](50) NOT NULL,
	[cost] [int] NOT NULL,
	[description] [nvarchar](1000) NULL,
	[imgPath] [varchar](500) NULL,
	[stationId] [int] NULL,
 CONSTRAINT [PK_Bike] PRIMARY KEY CLUSTERED 
(
	[barcode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[card]    Script Date: 1/4/2022 4:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[card](
	[cardholderName] [nvarchar](50) NULL,
	[cardNumber] [nvarchar](50) NOT NULL,
	[expirationDate] [varchar](50) NULL,
	[securityCode] [int] NULL,
	[money] [int] NULL,
	[bankId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[cardNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[giaoDich]    Script Date: 1/4/2022 4:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[giaoDich](
	[id] [int] NULL,
	[cardNumber] [varchar](50) NULL,
	[cardHolder] [varchar](50) NULL,
	[Time] [varchar](50) NULL,
	[Value] [int] NULL,
	[Message] [varchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RentalBike]    Script Date: 1/4/2022 4:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RentalBike](
	[carId] [int] NULL,
	[barcode] [varchar](20) NULL,
	[dateRentBike] [datetime] NULL,
	[dateReturnBike] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[station]    Script Date: 1/4/2022 4:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[station](
	[stationId] [int] NOT NULL,
	[stationName] [nvarchar](30) NOT NULL,
	[stationAddress] [nvarchar](60) NOT NULL,
	[numberOfSingleBike] [int] NOT NULL,
	[numberOfEBike] [int] NOT NULL,
	[numberOfTwinBike] [int] NOT NULL,
	[numberOfEmptyDocks] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[stationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[bank] ([name], [id]) VALUES (N'VpBank', 1)
INSERT [dbo].[bank] ([name], [id]) VALUES (N'Agribank', 2)
INSERT [dbo].[bank] ([name], [id]) VALUES (N'VietcomBank', 3)
GO
INSERT [dbo].[Bike] ([barcode], [name], [type], [weight], [licensePlate], [manuafacturingDate], [producer], [cost], [description], [imgPath], [stationId]) VALUES (N'00000001', N'VinBike', N'TwinBike', 70, N'37G1-88888', CAST(N'2020-10-15' AS Date), N'VinGroup', 3500000, N'Xe đạp đôi thường (TwinBike) có 2 yên, 2 bàn đạp và 1 ghế ngồi phía sau.', N'C:\\Users\\ducma\\IdeaProjects\\Ecobike\\src\\Img\\bike\\TwinBike.jpg', 100002)
INSERT [dbo].[Bike] ([barcode], [name], [type], [weight], [licensePlate], [manuafacturingDate], [producer], [cost], [description], [imgPath], [stationId]) VALUES (N'00000002', N'Xmen', N'EBike', 150, N'37G1-6666', CAST(N'2020-12-15' AS Date), N'DatGroup', 15500000, N'Xe đạp đơn điện (EBike) giống xe đạp đơn thường nhưng có motor điện giúp đạp xe nhanh hơn', N'C:\Users\ducma\IdeaProjects\Ecobike\src\Img\bike\EBike.jpg', 100002)
INSERT [dbo].[Bike] ([barcode], [name], [type], [weight], [licensePlate], [manuafacturingDate], [producer], [cost], [description], [imgPath], [stationId]) VALUES (N'00000003', N'JapanBike', N'SingleBike', 80, N'37G1-6868', CAST(N'2021-03-15' AS Date), N'JapanGroup', 15500000, N'Xe đạp đơn thường (SingleBike) chỉ có 1 yên, 1 bàn đạp và 1 ghế ngồi phía sau', N'C:\Users\ducma\IdeaProjects\Ecobike\src\Img\bike\SingleBike.jpg', 100002)
INSERT [dbo].[Bike] ([barcode], [name], [type], [weight], [licensePlate], [manuafacturingDate], [producer], [cost], [description], [imgPath], [stationId]) VALUES (N'00000004', N'Xmen', N'TwinBike', 70, N'37G1-66666', CAST(N'2021-02-16' AS Date), N'VinGroup', 3500000, N'Xe đạp đôi thường (TwinBike) có 2 yên, 2 bàn đạp và 1 ghế ngồi phía sau.', N'C:\\Users\\ducma\\IdeaProjects\\Ecobike\\src\\Img\\bike\\TwinBike.jpg', NULL)
INSERT [dbo].[Bike] ([barcode], [name], [type], [weight], [licensePlate], [manuafacturingDate], [producer], [cost], [description], [imgPath], [stationId]) VALUES (N'00000005', N'Xmen', N'EBike', 150, N'37A1-68686', CAST(N'2021-05-15' AS Date), N'DatGroup', 15300000, N'Xe đạp đơn điện (EBike) giống xe đạp đơn thường nhưng có motor điện giúp đạp xe nhanh hơn', N'C:\Users\ducma\IdeaProjects\Ecobike\src\Img\bike\EBike.jpg', 100001)
INSERT [dbo].[Bike] ([barcode], [name], [type], [weight], [licensePlate], [manuafacturingDate], [producer], [cost], [description], [imgPath], [stationId]) VALUES (N'00000006', N'Xmen', N'SingleBike', 80, N'37A1-86868', CAST(N'2021-05-20' AS Date), N'JapanGroup', 2000000, N'Xe đạp đơn thường (SingleBike) chỉ có 1 yên, 1 bàn đạp và 1 ghế ngồi phía sau', N'C:\Users\ducma\IdeaProjects\Ecobike\src\Img\bike\SingleBike.jpg', 100001)
INSERT [dbo].[Bike] ([barcode], [name], [type], [weight], [licensePlate], [manuafacturingDate], [producer], [cost], [description], [imgPath], [stationId]) VALUES (N'00000007', N'Xmen', N'SingleBike', 80, N'37G1-88888', CAST(N'2021-04-20' AS Date), N'JapanGroup', 2000000, N'Xe đạp đơn thường (SingleBike) chỉ có 1 yên, 1 bàn đạp và 1 ghế ngồi phía sau', N'C:\Users\ducma\IdeaProjects\Ecobike\src\Img\bike\SingleBike.jpg', 100001)
INSERT [dbo].[Bike] ([barcode], [name], [type], [weight], [licensePlate], [manuafacturingDate], [producer], [cost], [description], [imgPath], [stationId]) VALUES (N'00000008', N'Xmen', N'SingleBike', 80, N'37G1-69969', CAST(N'2021-01-20' AS Date), N'JapanGroup', 2000000, NULL, NULL, NULL)
GO
INSERT [dbo].[card] ([cardholderName], [cardNumber], [expirationDate], [securityCode], [money], [bankId]) VALUES (N'Nam', N'77777777', N'2021-10-10', 222222, 20000, 3)
INSERT [dbo].[card] ([cardholderName], [cardNumber], [expirationDate], [securityCode], [money], [bankId]) VALUES (N'Long', N'88888888', N'2020-05-10', 111111, 2000000, 2)
INSERT [dbo].[card] ([cardholderName], [cardNumber], [expirationDate], [securityCode], [money], [bankId]) VALUES (N'NguyenDucManh', N'99999999', N'2020-05-15', 123456, 1000000, 1)
GO
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000007', CAST(N'2021-12-11T15:30:00.000' AS DateTime), CAST(N'2021-12-25T00:00:00.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000001', CAST(N'2021-12-16T15:01:01.000' AS DateTime), CAST(N'2021-12-25T09:27:57.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000005', CAST(N'2021-12-16T15:02:27.000' AS DateTime), CAST(N'2021-12-25T09:43:43.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000007', CAST(N'2021-12-25T09:28:48.000' AS DateTime), CAST(N'2021-12-31T09:50:23.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000003', CAST(N'2021-12-25T09:38:29.000' AS DateTime), CAST(N'2021-12-25T09:39:02.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000005', CAST(N'2021-12-25T09:45:07.000' AS DateTime), CAST(N'2021-12-31T09:46:40.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000001', CAST(N'2021-12-25T09:45:22.000' AS DateTime), CAST(N'2021-12-25T09:46:21.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000002', CAST(N'2021-12-25T09:45:40.000' AS DateTime), CAST(N'2021-12-25T09:46:49.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000006', CAST(N'2021-12-25T09:45:57.000' AS DateTime), CAST(N'2021-12-25T09:47:43.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000002', CAST(N'2021-12-30T07:52:28.000' AS DateTime), CAST(N'2021-12-30T07:52:51.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000002', CAST(N'2021-12-30T08:05:49.000' AS DateTime), CAST(N'2021-12-30T08:06:05.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000001', CAST(N'2021-12-30T08:15:50.000' AS DateTime), CAST(N'2021-12-30T08:16:06.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000002', CAST(N'2021-12-30T08:36:56.000' AS DateTime), CAST(N'2021-12-30T08:37:12.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000006', CAST(N'2021-12-30T10:02:32.000' AS DateTime), CAST(N'2021-12-30T10:04:02.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000004', CAST(N'2021-12-30T10:06:14.000' AS DateTime), CAST(N'2021-12-31T09:45:50.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000006', CAST(N'2021-12-30T10:35:25.000' AS DateTime), CAST(N'2021-12-30T10:35:39.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000001', CAST(N'2022-01-03T09:19:05.000' AS DateTime), CAST(N'2022-01-03T09:19:28.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000004', CAST(N'2022-01-04T08:40:39.000' AS DateTime), NULL)
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000003', CAST(N'2022-01-04T08:40:57.000' AS DateTime), CAST(N'2022-01-04T09:55:52.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000002', CAST(N'2021-12-16T15:22:34.000' AS DateTime), CAST(N'2021-12-25T09:37:35.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000003', CAST(N'2021-12-16T15:22:48.000' AS DateTime), CAST(N'2021-12-25T09:38:12.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000001', CAST(N'2021-12-30T08:18:52.000' AS DateTime), CAST(N'2021-12-30T10:50:43.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000002', CAST(N'2021-12-30T08:42:55.000' AS DateTime), CAST(N'2021-12-30T10:29:16.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000003', CAST(N'2021-12-30T09:33:47.000' AS DateTime), CAST(N'2021-12-30T09:34:03.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000003', CAST(N'2021-12-30T09:44:36.000' AS DateTime), CAST(N'2021-12-30T10:30:50.000' AS DateTime))
INSERT [dbo].[RentalBike] ([carId], [barcode], [dateRentBike], [dateReturnBike]) VALUES (99999999, N'00000006', CAST(N'2021-12-30T09:49:51.000' AS DateTime), CAST(N'2021-12-30T09:50:14.000' AS DateTime))
GO
INSERT [dbo].[station] ([stationId], [stationName], [stationAddress], [numberOfSingleBike], [numberOfEBike], [numberOfTwinBike], [numberOfEmptyDocks]) VALUES (100001, N'S1', N'Sapphire1 - Ecopark - Van Giang - Hung Yên', 2, 1, 0, 47)
INSERT [dbo].[station] ([stationId], [stationName], [stationAddress], [numberOfSingleBike], [numberOfEBike], [numberOfTwinBike], [numberOfEmptyDocks]) VALUES (100002, N'S2', N'Sapphire2 - Ecopark - Van Giang - Hung Yên', 1, 1, 1, 47)
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__station__6FA83082377FE7A0]    Script Date: 1/4/2022 4:03:12 PM ******/
ALTER TABLE [dbo].[station] ADD UNIQUE NONCLUSTERED 
(
	[stationName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__station__8D0EE673FA70D4B4]    Script Date: 1/4/2022 4:03:12 PM ******/
ALTER TABLE [dbo].[station] ADD UNIQUE NONCLUSTERED 
(
	[stationAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Bike]  WITH CHECK ADD FOREIGN KEY([stationId])
REFERENCES [dbo].[station] ([stationId])
GO
ALTER TABLE [dbo].[RentalBike]  WITH CHECK ADD FOREIGN KEY([barcode])
REFERENCES [dbo].[Bike] ([barcode])
GO
