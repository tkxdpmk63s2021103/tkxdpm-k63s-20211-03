package view.admin;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.Admin;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class AdminHandle {
	private static Boolean loginAsAdmin = false;
	
	public static Boolean hasLoginAsAdmin() {
		return loginAsAdmin;
	}
	
	@FXML
    private TextField AccountTextField = new TextField();
	
	@FXML
    private PasswordField PasswordTextField = new PasswordField();
	
	public void loginAdmin(ActionEvent event) throws IOException {
		String account = AccountTextField.getText();
		String pass = PasswordTextField.getText();
		if(account == "" || pass == "") {
			showAlert("Dien thieu truong");
			return;
		}
		
		//query trong database
		if(Admin.authenticateAccount(account, pass)) {
			AdminHandle.loginAsAdmin = true;
			System.out.println(AdminHandle.loginAsAdmin);
			showAlert("Dang nhap thanh cong");
			backToHome(event);
		}else {
			AdminHandle.loginAsAdmin = false;
			showAlert("Dang nhap tu choi");
		}
	}
	
	public void backToHome(ActionEvent event) throws IOException {
        Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/home/HomeScreen.fxml"));
        Parent view = loader.load();
        Scene scene = new Scene(view);
        stage.setScene(scene);
    }
	
	public void handleReturnBikeClick(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage) ( (Node) actionEvent.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/viewtraxe/EnterBarcode.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        stage.setScene(scene);
    }
	
	private void showAlert(String text) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Admin Login");
		alert.setContentText(text);

		alert.showAndWait();
	}
}
