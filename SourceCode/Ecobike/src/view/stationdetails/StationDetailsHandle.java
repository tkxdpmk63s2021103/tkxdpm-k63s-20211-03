package view.stationdetails;

import controller.DataController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Bike;
import model.Station;
import view.viewmuonxe.BikeDetailsHandle;
import view.viewmuonxe.DescriptionRentBikeHandle;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class StationDetailsHandle {
    @FXML
    Label stationId = new Label();

    @FXML
    Label name = new Label();

    @FXML
    Label address = new Label();

    @FXML
    Label numberOfSingleBike = new Label();

    @FXML
    Label numberOfTwinBike = new Label();

    @FXML
    Label numberOfEBike = new Label();

    @FXML
    Label numberOfEmpty = new Label();

    @FXML
    private TableView<Bike> tableView;

    @FXML
    TableColumn<Bike, String> barcode = new TableColumn();

    @FXML
    TableColumn<Bike,String> bikeName = new TableColumn<>();

    @FXML
    TableColumn<Bike,String> type = new TableColumn<>();

    @FXML
    TableColumn<Bike,String> licensePlate = new TableColumn<>();

    @FXML
    TableColumn<Bike,String> producer = new TableColumn<>();

    @FXML
    ImageView imgStation = new ImageView();

    private ObservableList<Bike> bikesOb;
    private DataController dataController = new DataController();
    private Station station = dataController.getStationFromId(100001);



    public void  setData(Station station){
        stationId.setText(String.valueOf(station.getStationId()));
        name.setText(station.getStationName());
        address.setText(station.getStationAddress());
        numberOfSingleBike.setText(String.valueOf(station.getNumberOfSingleBike()));
        numberOfTwinBike.setText(String.valueOf(station.getNumberOfTwinBike()));
        numberOfEBike.setText(String.valueOf(station.getNumberOfEBike()));
        numberOfEmpty.setText(String.valueOf(station.getNumberOfEmptyDocks()));
        File file = new File("C:\\Users\\ducma\\IdeaProjects\\Ecobike\\src\\Img\\station.jpg");
        Image image = null;
        try {
            image = new Image(file.toURI().toURL().toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        imgStation.setImage(image);

        try {
            ArrayList<Bike> bikes = dataController.getBikeFromStation(station.getStationId());
            bikesOb = FXCollections.observableArrayList(bikes);

            barcode.setCellValueFactory(new PropertyValueFactory<Bike,String>("barcode"));
            bikeName.setCellValueFactory(new PropertyValueFactory<Bike,String>("name"));
            type.setCellValueFactory(new PropertyValueFactory<Bike,String>("type"));
            licensePlate.setCellValueFactory(new PropertyValueFactory<Bike,String>("licensePlate"));
            producer.setCellValueFactory(new PropertyValueFactory<Bike,String>("producer"));



            tableView.setItems(bikesOb);
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Danh sách rỗng.");
            alert.show();
        }
    }

    public void selectBike(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) ( (Node) mouseEvent.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/viewmuonxe/BikeDetails.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        BikeDetailsHandle controller = loader.getController();
        controller.setInfoBike(tableView.getSelectionModel().getSelectedItem());
        stage.setScene(scene);
    }

    public void back(ActionEvent event) throws IOException {
        Stage stage = (Stage) ( (Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/home/HomeScreen.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        stage.setScene(scene);

    }
}
