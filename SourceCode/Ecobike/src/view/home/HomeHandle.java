package view.home;

import controller.DataController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Station;
import view.Pay.BaseHandler;
import view.admin.AdminHandle;
import view.bikemanagement.BikeManagementHandler;
import view.stationdetails.StationDetailsHandle;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class HomeHandle implements Initializable {


    @FXML
    TableView tableView = new TableView();

    @FXML
    TextField searchInput = new TextField();

    @FXML
    TableColumn<Station,String> name = new TableColumn<>();

    @FXML
    TableColumn<Station,String> address = new TableColumn<>();

    @FXML
    TableColumn<Station,Integer> singleBike = new TableColumn<>();

    @FXML
    TableColumn<Station,Integer> eBike = new TableColumn<>();

    @FXML
    TableColumn<Station,Integer> twinBike = new TableColumn<>();

    private DataController dataController = new DataController();

    private ObservableList<Station> stationsOb;

    public void handleHomeClick(ActionEvent actionEvent) {
    }

    public void handleReturnBikeClick(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage) ( (Node) actionEvent.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/viewtraxe/EnterBarcode.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        stage.setScene(scene);
    }

    public void handleHelpClick(ActionEvent actionEvent) {
    }

    public void handleFeedbackClick(ActionEvent actionEvent) {
    }
    
    public void handleAdminClick(ActionEvent actionEvent) throws IOException {
    	Stage stage = (Stage) ( (Node) actionEvent.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/admin/AdminView.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        stage.setScene(scene);
    }
    
    public void handleBikeManageClick(ActionEvent actionEvent) throws IOException {
    	if (AdminHandle.hasLoginAsAdmin()) {
    		Stage stage = (Stage) ( (Node) actionEvent.getSource()).getScene().getWindow();
        	BaseHandler handler = new BikeManagementHandler(stage, "/view/bikemanagement/BikeManagement.fxml");
            handler.show();
    	} else {
    		Alert alert = new Alert(Alert.AlertType.INFORMATION);
    		alert.setHeaderText("Thông báo");
    		alert.setContentText("Cần đăng nhập với vai trò Admin trước khi truy cập");
    		alert.show();
    	}
    }

    public void handleSearchEnter(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)) {
            String input = searchInput.getText();

            if (!input.trim().isEmpty()) {
                ArrayList<Station> listNewStation = dataController.searchStation(input);
                ObservableList<Station> stationsOb = FXCollections.observableArrayList(listNewStation);
                tableView.setItems(stationsOb);
                tableView.refresh();
            }
        }
    }

    public void handleStationSelected(MouseEvent mouseEvent) {
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        try {
            ArrayList<Station> stations = dataController.getListStation();
            stationsOb = FXCollections.observableArrayList(stations);

            name.setCellValueFactory(new PropertyValueFactory<Station,String>("stationName"));
            address.setCellValueFactory(new PropertyValueFactory<Station,String>("stationAddress"));
            singleBike.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfSingleBike"));
            eBike.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfEBike"));
            twinBike.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfTwinBike"));

            tableView.setItems(stationsOb);
        } catch (Exception e) {
          e.printStackTrace();
        }

    }

    public void showStationDetails(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) ( (Node) mouseEvent.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/stationdetails/StationDetails.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        StationDetailsHandle controller = loader.getController();
        controller.setData((Station) tableView.getSelectionModel().getSelectedItem());
        stage.setScene(scene);
    }

    public void back(ActionEvent event) throws IOException {
        Stage stage = (Stage) ( (Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/home/HomeScreen.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        stage.setScene(scene);
    }
}
