package view.bikemanagement;

import controller.DataController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Bike;
import model.Station;
import view.Pay.BaseHandler;
import view.bikemanagement.bikeadd.BikeAddHandler;
import view.bikemanagement.bikeeditor.BikeEditorHandler;
import view.viewmuonxe.BikeDetailsHandle;
import view.viewmuonxe.DescriptionRentBikeHandle;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

public class BikeManagementHandler extends BaseHandler {
	public BikeManagementHandler(Stage stage, String fxmlPath) throws IOException {
		super(stage, fxmlPath);
		this.setData();
	}

	@FXML
	private TextField searchInput;

	@FXML
	private Button searchBtn;

	@FXML
	private Button addBtn;

	@FXML
	private TableView<Bike> tableView;

	@FXML
	TableColumn<Bike, String> barcode;

	@FXML
	TableColumn<Bike, String> bikeName;

	@FXML
	TableColumn<Bike, String> type;

	@FXML
	TableColumn<Bike, String> licensePlate;

	@FXML
	TableColumn<Bike, String> producer;
	
	@FXML
	TableColumn<Bike, String> stationName;

	private ObservableList<Bike> bikesOb;
	private DataController dataController = new DataController();

	/**
	 * Get and render bike list data
	 */
	public void setData() {
		try {
			// Get station list
			ArrayList<Station> stations = Station.getListStation();
			// Create station map
			HashMap<Integer, String> stationMap = new HashMap<Integer, String>();
			stations.forEach(e -> {
				stationMap.put(e.getStationId(), e.getStationName());
			});
			// Add station name to bike
			ArrayList<Bike> bikes = dataController.getAllBikes();
			bikes.forEach(e -> {
				int stationId = e.getStationId();
				e.setStationName(stationId != 0 ? stationMap.get(e.getStationId()) : "Đang thuê");
			});
			// Render table
			bikesOb = FXCollections.observableArrayList(bikes);

			barcode.setCellValueFactory(new PropertyValueFactory<Bike, String>("barcode"));
			bikeName.setCellValueFactory(new PropertyValueFactory<Bike, String>("name"));
			type.setCellValueFactory(new PropertyValueFactory<Bike, String>("type"));
			licensePlate.setCellValueFactory(new PropertyValueFactory<Bike, String>("licensePlate"));
			producer.setCellValueFactory(new PropertyValueFactory<Bike, String>("producer"));
			stationName.setCellValueFactory(new PropertyValueFactory<Bike, String>("stationName"));

			tableView.setItems(bikesOb);
			
			addBtn.setOnMouseClicked(e -> addBike(e));
		} catch (Exception e) {
			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setContentText("Danh sách xe rỗng.");
			alert.show();
		}
	}

	/**
	 * Display a bike information
	 * 
	 * @param mouseEvent
	 */
	public void selectBike(MouseEvent mouseEvent) {
		try {
			Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
			BaseHandler handler = new BikeEditorHandler(stage, "/view/bikemanagement/bikeeditor/BikeEditor.fxml",
					tableView.getSelectionModel().getSelectedItem());
			handler.show();
		} catch (Exception e) {
			e.printStackTrace();
			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setContentText("Không thể hiển thị thông tin xe.");
			alert.show();
			this.setData();
		}
	}
	
	/**
	 * Show add bike screen
	 * @param mouseEvent
	 */
	public void addBike(MouseEvent mouseEvent) {
		try {
			Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
			BaseHandler handler = new BikeAddHandler(stage, "/view/bikemanagement/bikeadd/BikeAdd.fxml");
			handler.show();
		} catch (Exception e) {
			e.printStackTrace();
			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setContentText("Có lỗi xảy ra, không thể mở cửa sổ thêm xe.");
			alert.show();
			this.setData();
		}
	}

	/**
	 * Return to home scene
	 * 
	 * @param event Event
	 * @throws IOException
	 */
	public void back(ActionEvent event) throws IOException {
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/home/HomeScreen.fxml"));
		Parent parent = loader.load();
		Scene scene = new Scene(parent);
		stage.setScene(scene);

	}
}
