package view.viewtraxe;

import controller.DataController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Bike;
import model.RentalBike;
import model.Station;

import static view.Pay.TestPayHandle.monney;
import static view.Pay.TestPayHandle.statusPay;
import static view.viewtraxe.EnterBarcodeHandle.rentalBike;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ChooseStationHandle {

    @FXML
    TableView<Station> tableView = new TableView();

    @FXML
    TableColumn<Station,String> name = new TableColumn<>();

    @FXML
    TableColumn<Station,String> address = new TableColumn<>();

    @FXML
    TableColumn<Station,Integer> singleBike = new TableColumn<>();

    @FXML
    TableColumn<Station,Integer> eBike = new TableColumn<>();

    @FXML
    TableColumn<Station,Integer> twinBike = new TableColumn<>();

    @FXML
    TableColumn<Station,Integer> emptyDocks = new TableColumn<>();

    private DataController dataController = new DataController();

    private ObservableList<Station> stationsOb;

    public static Station stationSelected;

    public void setListStation() {
        try {
            ArrayList<Station> stations = dataController.getListStation();
            stationsOb = FXCollections.observableArrayList(stations);

            name.setCellValueFactory(new PropertyValueFactory<Station,String>("stationName"));
            address.setCellValueFactory(new PropertyValueFactory<Station,String>("stationAddress"));
            singleBike.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfSingleBike"));
            eBike.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfEBike"));
            twinBike.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfTwinBike"));
            emptyDocks.setCellValueFactory(new PropertyValueFactory<Station,Integer>("numberOfEmptyDocks"));
            tableView.setItems(stationsOb);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void backToRentalBikeDetails(ActionEvent event) throws IOException {
        DataController dataController = new DataController();
        if(rentalBike != null){
            Stage stage = (Stage) ( (Node) event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/viewtraxe/RentalBikeDetails.fxml"));
            Parent parent = loader.load();
            Scene scene = new Scene(parent);
            RentalBikeDetailsHandle controller = loader.getController();
            controller.setInfoRentalBike();
            stage.setScene(scene);
        }else{
            System.out.println("Xe đã được mượn..");
        }
    }

    public void handleStationSelected(MouseEvent event) throws IOException {
        stationSelected = tableView.getSelectionModel().getSelectedItem();
        Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/Pay/PayemntScreen.fxml"));
        Parent view = loader.load();
        statusPay = 0;
        monney = 0;
        Scene scene = new Scene(view);
        stage.setScene(scene);
    }
}
