package view.viewtraxe;

import controller.ReturnBikeController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Bike;
import model.RentalBike;
import view.stationdetails.StationDetailsHandle;

import java.io.IOException;

public class EnterBarcodeHandle {

    public static RentalBike rentalBike;
    @FXML
    private TextField barcodeTextField = new TextField();

    public void searchRentalBike(ActionEvent event) throws IOException {
        rentalBike = RentalBike.getRentalBikeFromBarcode(barcodeTextField.getText());
        if(rentalBike.getBike() != null){
            System.out.println("RentalBike tồn tại");
            Stage stage = (Stage) ( (Node) event.getSource()).getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/viewtraxe/RentalBikeDetails.fxml"));
            Parent parent = loader.load();
            Scene scene = new Scene(parent);
            RentalBikeDetailsHandle controller = loader.getController();
            controller.setInfoRentalBike();
            stage.setScene(scene);
        }else{
            System.out.println("Xe đã được mượn..");
        }
    }

    public void backToHome(ActionEvent event) throws IOException {
        Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/home/HomeScreen.fxml"));
        Parent view = loader.load();
        Scene scene = new Scene(view);
        stage.setScene(scene);
    }
    
    public void handleAdminClick(ActionEvent actionEvent) throws IOException {
    	Stage stage = (Stage) ( (Node) actionEvent.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/admin/AdminView.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        stage.setScene(scene);
    }
}
