package view.viewtraxe;

import controller.ReturnBikeController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Bike;
import model.RentalBike;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import static view.viewtraxe.EnterBarcodeHandle.rentalBike;
import static view.Pay.TestPayHandle.monney;
public class RentalBikeDetailsHandle {
    @FXML
    Label Barcode = new Label();

    @FXML
    Label Name = new Label();

    @FXML
    Label Type = new Label();

    @FXML
    Label Weight = new Label();

    @FXML
    Label LicensePlate = new Label();

    @FXML
    Label ManufacturingDate = new Label();

    @FXML
    Label Producer = new Label();

    @FXML
    Label textPay = new Label();

    @FXML
    Label rentBikeDay = new Label();

    @FXML
    Label moneyToPay = new Label();

    @FXML
    ImageView ViewBike = new ImageView();

    private ReturnBikeController returnBikeController = new ReturnBikeController();

    public void setInfoRentalBike() throws MalformedURLException {
        Barcode.setText(rentalBike.getBike().getBarcode());
        Name.setText(rentalBike.getBike().getName());
        Type.setText(rentalBike.getBike().getType());
        Weight.setText(String.valueOf(rentalBike.getBike().getWeight()));
        LicensePlate.setText(rentalBike.getBike().getLicensePlate());
        ManufacturingDate.setText(String.valueOf(rentalBike.getBike().getManufacturingDate()));
        Producer.setText(rentalBike.getBike().getProducer());
        rentBikeDay.setText(rentalBike.getStartTime().toString());
        ReturnBikeController returnBikeController = new ReturnBikeController();
        int moneyPay = returnBikeController.calculateRentalFees(rentalBike);
        if(moneyPay > 0){
            textPay.setText("Cần thanh toán");
        }else{
            textPay.setText("Hoàn tiền");
        }
        moneyToPay.setText(Math.abs((returnBikeController.calculateRentalFees(rentalBike)))+"VND");
        monney = moneyPay;
        File file = new File(rentalBike.getBike().getImgPath());
        Image image = new Image(file.toURI().toURL().toString());
        ViewBike.setImage(image);
    }


    public void confirmReturnBike(ActionEvent event) throws IOException {
        Stage stage = (Stage) ( (Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/viewtraxe/ChooseStation.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        ChooseStationHandle controller = loader.getController();
        controller.setListStation();
        stage.setScene(scene);
    }

    public void backToEnterBarcode(ActionEvent event) throws IOException {
        Stage stage = (Stage) ( (Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/viewtraxe/EnterBarcode.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        stage.setScene(scene);
    }
}
