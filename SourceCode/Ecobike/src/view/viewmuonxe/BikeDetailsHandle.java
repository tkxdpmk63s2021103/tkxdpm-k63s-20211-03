package view.viewmuonxe;


import controller.DataController;
import controller.RentBikeController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Admin;
import model.Bike;
import view.admin.AdminHandle;
import view.stationdetails.StationDetailsHandle;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;



public class BikeDetailsHandle {
    @FXML
    Label Barcode = new Label();

    @FXML
    Label Name = new Label();

    @FXML
    Label Type = new Label();

    @FXML
    Label Weight = new Label();

    @FXML
    Label LicensePlate = new Label();

    @FXML
    Label ManufacturingDate = new Label();

    @FXML
    Label Producer = new Label();

    @FXML
    Label Cost = new Label();

    @FXML
    Label Description = new Label();

    @FXML
    ImageView ViewBike = new ImageView();

    private DataController dataController = new DataController();

    public static Bike bikeSelected;

    public void setInfoBike(Bike bike) throws MalformedURLException {
        this.bikeSelected = bike;
        System.out.println("Details : "+bike.getBarcode());
        Barcode.setText(bike.getBarcode());
        Name.setText(bike.getName());
        Type.setText(bike.getType());
        Weight.setText(String.valueOf(bike.getWeight()));
        LicensePlate.setText(bike.getLicensePlate());
        ManufacturingDate.setText(String.valueOf(bike.getManufacturingDate()));
        Producer.setText(bike.getProducer());
        Cost.setText(String.valueOf(bike.getCost()));
        Description.setText(bike.getDescription());
        File file = new File(bike.getImgPath());
        Image image = new Image(file.toURI().toURL().toString());
        ViewBike.setImage(image);
    }


    public void backToStationDetails(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage)((Node) actionEvent.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/stationdetails/StationDetails.fxml"));
        Parent view = loader.load();
        Scene scene = new Scene(view);
        StationDetailsHandle cotroller = loader.getController();
        cotroller.setData(dataController.getStationFromId(bikeSelected.getStationId()));
        stage.setScene(scene);
    }


    public void nextToDescriptionRentBike(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage) ( (Node) actionEvent.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/viewmuonxe/DescriptionRentBike.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        DescriptionRentBikeHandle controller = loader.getController();
        String desctipton  = "Với xe đạp đơn thường, nếu khách hàng dùng xe hơn 10 phút, phí thuê xe được tính lũy tiến theo thời gian thuê như sau: Giá khởi điểm cho 30 phút đầu là 10.000 đồng. Cứ mỗi 15 phút tiếp theo, khách sẽ phải trả thêm 3.000 đồng. Ví dụ, khách thuê 1 tiếng 10 phút cần trả 10.000 + 3x3.000 = 19.000 đồng.\n" +
                "Phí thuê xe đạp đơn điện và xe đạp đôi thường gấp 1.5 lần so với phí thuê xe đạp đơn thường.";
        controller.setData(desctipton);
        stage.setScene(scene);
    }
    
    public void deleteBike(ActionEvent actionEvent) throws IOException {
    	//check if admin
    	if(!AdminHandle.hasLoginAsAdmin()) {
    		showAlert("Admin Authentication", "Ban khong co quyen admin");
    		return;
    	}
    	
    	// delete record from database
    	if(Bike.deleteBikeFromBarcode(BikeDetailsHandle.bikeSelected.getBarcode())){
    		showAlert("Bike Delete", "Xoa thanh cong");
    		backToStationDetails(actionEvent);
    	}else {
    		showAlert("Bike Delete", "Xoa that bai");
    	}
    }
    
    private void showAlert(String title, String text) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(title);
		alert.setContentText(text);

		alert.showAndWait();
	}
}
