package view.viewmuonxe;

import controller.PaymentConfig;
import controller.PaymentController;
import controller.RentBikeController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import view.Pay.BaseHandler;
import view.Pay.PaymentHandler;

import static view.Pay.TestPayHandle.statusPay;
import static view.Pay.TestPayHandle.monney;

import static view.viewmuonxe.BikeDetailsHandle.bikeSelected;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;


public class DescriptionRentBikeHandle extends BaseHandler{

    public DescriptionRentBikeHandle(String fxmlPath) throws IOException {
		super(fxmlPath);
	}

	@FXML
    private Label Deposit = new Label();

    @FXML
    private Label Description = new Label();

    @FXML
    private Label barcode = new Label();

    @FXML
    private ImageView img;


    public void setData(String message) throws MalformedURLException {
        RentBikeController rentBikeController = new RentBikeController();
        int deposit = rentBikeController.calculateDeposit(bikeSelected);
        Deposit.setText(String.valueOf(deposit));
        Description.setText(message);
        barcode.setText(bikeSelected.getBarcode());
        File file = new File(bikeSelected.getImgPath());
        Image image = new Image(file.toURI().toURL().toString());
        img.setImage(image);
    }

    public void displayError(String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText(message);
        alert.show();
    }

    public void nextToPay(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage)((Node) actionEvent.getSource()).getScene().getWindow();
//        FXMLLoader loader = new FXMLLoader();
//        loader.setLocation(getClass().getResource("/view/Pay/PaymentScreen.fxml"));
//        Parent view = loader.load();
//        statusPay = 1;
//        monney = Integer.valueOf(Deposit.getText());
//        Scene scene = new Scene(view);
//        stage.setScene(scene);
    	BaseHandler paymentScene =  new PaymentHandler("/view/Pay/PaymentScreen.fxml", "/view/CSS/paymentStyle.css", PaymentConfig.PAY, 10000, "Giao dich thu tien xe bla bla");
    	paymentScene.setStage(stage);
    	paymentScene.show();
    }

    public void backToBikeDetails(ActionEvent event) throws IOException {
        Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/viewmuonxe/BikeDetails.fxml"));
        Parent view = loader.load();
        Scene scene = new Scene(view);
        BikeDetailsHandle cotroller = loader.getController();
        cotroller.setInfoBike(bikeSelected);
        stage.setScene(scene);
    }
}
