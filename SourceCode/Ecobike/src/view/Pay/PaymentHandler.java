package view.Pay;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import controller.PaymentController;
import exceptions.PaymentException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Bank;
import model.CreditCard;

public class PaymentHandler extends BaseHandler {
	
	private int payType;
	private int amount;
	private String message;
	private HashMap<String, Integer> bankMap;

	static {
		try {
			Class.forName("controller.PayController");
			Class.forName("controller.RefundController");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private ChoiceBox<String> bankChoiceBox;
	
	@FXML
	private TextField cardNumber;
	
	@FXML
	private TextField cardHolder;
	
	@FXML
	private TextField expirationDate;
	
	@FXML
	private TextField cvc;
	
	@FXML
	private Button confirmBtn;
	
	@FXML
	private Button cancelBtn;
	
	private void init() {
		bankMap = new HashMap<String, Integer>();
		setController(new PaymentController());
		
		Iterator<Bank> banks = Bank.getAll();
		while(banks.hasNext()) {
			Bank bank = banks.next();
			bankMap.put(bank.getName(), bank.getId());
			bankChoiceBox.getItems().add(bank.getName());
		}
		
		confirmBtn.setOnMouseClicked(e -> confirm(e));
		confirmBtn.setOnMouseEntered(e -> buttonMouseEnter(e));
		confirmBtn.setOnMouseExited(e -> buttonmMouseExit(e));
		
		cancelBtn.setOnMouseClicked(e -> cancel(e));
		cancelBtn.setOnMouseEntered(e -> buttonMouseEnter(e));
		cancelBtn.setOnMouseExited(e -> buttonmMouseExit(e));
	}
	
	public PaymentHandler(String fxmlPath, int payType, int amount, String message) throws IOException {
		super(fxmlPath);
		this.payType = payType;
		this.amount = amount;
		this.message = message;
		init();
	
	}
	
	public PaymentHandler(String fxmlPath, String cssPath, int payType, int amount, String message) throws IOException {
		super(fxmlPath, cssPath);
		this.payType = payType;
		this.amount = amount;
		this.message = message;
		init();
	}
	
	
	public void confirm(MouseEvent e) {
		PaymentController controller = (PaymentController)getController();
		try {
			CreditCard card = new CreditCard();
			card.setCardNumber(cardNumber.getText());
			card.setCardHolderName(cardHolder.getText());
			card.setExpirationDate(expirationDate.getText());
			card.setSecurityCode(Integer.parseInt(cvc.getText()));
			card.setBankId(bankMap.get(bankChoiceBox.getValue()));
			
			controller.setCard(card);
			controller.request(payType, amount, message);
			
			
			Stage stage = (Stage)((Node) e.getSource()).getScene().getWindow();
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(getClass().getResource("/view/viewmuonxe/ResultRentBike.fxml"));
	        
	        Parent view;
			try {
				 view = loader.load();
				 Scene scene = new Scene(view);
		         stage.setScene(scene);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
		} catch (PaymentException error) {
			try {
				BaseHandler resultScreen = new PaymentResultHandler(getStage(), "/view/Pay/PaymentResultScreen.fxml", error.getMessage());
				popUp(resultScreen);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
	}
	
	public void cancel(MouseEvent e) {
		switchBack();
	}
	
	public void buttonMouseEnter(MouseEvent e) {
		Button btn = (Button)e.getSource();
		btn.setStyle("-fx-background-color: #f2f2f0; -fx-border-width: 2px");
	}
	
	public void buttonmMouseExit(MouseEvent e) {
		Button btn = (Button)e.getSource();
		btn.setStyle("-fx-background-color: #ffffff; -fx-border-width: 2px");
	}
}
