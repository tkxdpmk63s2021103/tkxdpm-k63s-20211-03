package view.Pay;

import controller.RentBikeController;
import controller.ReturnBikeController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Card;
import view.stationdetails.StationDetailsHandle;

import java.io.IOException;

public class TestPayHandle {
    @FXML
    TextField Pass = new TextField();
    RentBikeController rentBikeController = new RentBikeController();
    ReturnBikeController returnBikeController = new ReturnBikeController();
    public static int statusPay;
    public static int monney;
    public static int cardId;
    public void clickButton(ActionEvent event) throws IOException {

        if(Pass.getText().equals("DucManh")){
            System.out.println("Thanh toán ok");
            if(statusPay == 1){
                cardId = 99999999;
                rentBikeController.updateDataAfterRentBike();
                Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/view/viewmuonxe/ResultRentBike.fxml"));
                Parent view = loader.load();
                Scene scene = new Scene(view);
                stage.setScene(scene);
            }else{
                returnBikeController.updateDataAfterReturnBike();
                Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/view/viewtraxe/ResultReturnBike.fxml"));
                Parent view = loader.load();
                Scene scene = new Scene(view);
                stage.setScene(scene);
            }
        }else{
            System.out.println("Thanh toán lỗi.Hiện thông báo");
        }
    }

}
