package view.Pay;

import java.util.Calendar;
import java.util.Date;

import exceptions.PaymentException;

public class CreditCardDateValidation{
	
	public static String validate(String expirationDate) throws PaymentException {
		Date now = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		
		String[] date = expirationDate.split("/");
		
		if(date[0].length() != 2 || date[1].length() != 2) {
			throw new RuntimeException("Expiration date is incorrect");
		}
		else {
			if (Integer.parseInt(date[1]) < year % 2000) {
				if(Integer.parseInt(date[0]) < month) {
					throw new PaymentException("Expiration date is incorrect");
				}
			}
		}
		
		return expirationDate;
		
	}
}
