package view.Pay;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class PaymentResultHandler extends BaseHandler {
	
	@FXML
	private Label message;
	
	@FXML 
	private Button closeBtn;
	
	public PaymentResultHandler(Stage stage, String fxmlPath, String message) throws IOException {
		super(stage, fxmlPath);
		
		this.message.setText(message);
		closeBtn.setOnMouseClicked(e -> close(e));
	}
	
	public void close(MouseEvent e) {
		super.close();
	}
	
}
