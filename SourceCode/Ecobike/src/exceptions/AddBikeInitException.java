package exceptions;

public class AddBikeInitException extends Exception {
	public AddBikeInitException(String message) {
		super(message);
	}
}
