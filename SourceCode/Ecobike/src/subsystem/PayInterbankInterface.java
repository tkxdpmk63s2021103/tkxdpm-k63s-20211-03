package subsystem;

import exceptions.PaymentException;
import model.Card;
import model.Transaction;

public interface PayInterbankInterface {
	public Transaction requestToPay(Card card, int amount, String message) throws PaymentException;
}
