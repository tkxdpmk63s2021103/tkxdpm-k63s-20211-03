package subsystem;

import exceptions.PaymentException;
import model.Card;
import model.CreditCard;
import model.Transaction;
import subsystem.interbank.PayInterbankController;

public class PayInterbankSubsystem implements PayInterbankInterface {
	private PayInterbankController controller;
	
	public PayInterbankSubsystem() {
		controller = new PayInterbankController();
	}

	public Transaction requestToPay(Card card, int amount, String message) throws PaymentException {
		controller.pay((CreditCard)card, amount, message);
		return null;
	}
}
