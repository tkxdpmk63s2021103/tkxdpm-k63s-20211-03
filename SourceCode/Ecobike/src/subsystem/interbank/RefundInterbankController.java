package subsystem.interbank;

import exceptions.PaymentException;
import model.CreditCard;

public class RefundInterbankController {
	private InterbankView view;
	
	public RefundInterbankController() {
		view = new InterbankView();
	}
	
	public void refund(CreditCard card, int amount, String message) throws PaymentException {
		view.post(card, amount, message, 1);
	}
}
