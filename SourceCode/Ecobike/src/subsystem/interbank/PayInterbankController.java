package subsystem.interbank;

import exceptions.PaymentException;
import model.CreditCard;

public class PayInterbankController {
	private InterbankView view;
	
	public PayInterbankController() {
		view = new InterbankView();
	}
	
	public void pay(CreditCard card, int amount, String message) throws PaymentException {
		view.post(card, amount, message, -1);
	}
}
