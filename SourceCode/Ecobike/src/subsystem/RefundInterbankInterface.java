package subsystem;

import exceptions.PaymentException;
import model.Card;
import model.Transaction;

public interface RefundInterbankInterface {
	public Transaction requestToRefund(Card card, int amount, String message) throws PaymentException;
}
