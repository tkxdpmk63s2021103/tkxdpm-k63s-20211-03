package subsystem;

import exceptions.PaymentException;
import model.Card;
import model.CreditCard;
import model.Transaction;
import subsystem.interbank.RefundInterbankController;

public class RefundInterbankSubsystem implements RefundInterbankInterface {

	private RefundInterbankController controller;
	
	public RefundInterbankSubsystem () {
		controller = new RefundInterbankController();
	}
	
	public Transaction requestToRefund(Card card, int amount, String message) throws PaymentException {
		controller.refund((CreditCard)card, amount, message);
		return null;
	}

}
