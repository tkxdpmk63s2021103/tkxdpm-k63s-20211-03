package controller;

import controller.CalculatorLogic.FeesCalculator01;
import controller.CalculatorLogic.FeesCalculatorInterface;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Bike;
import model.RentalBike;
import model.Station;
import view.viewtraxe.ChooseStationHandle;
import view.viewtraxe.RentalBikeDetailsHandle;

import static view.viewtraxe.EnterBarcodeHandle.rentalBike;
import static view.viewtraxe.ChooseStationHandle.stationSelected;
import java.io.IOException;

public class ReturnBikeController {


    public int calculateRentalFees(RentalBike rentalBike) {
        FeesCalculatorInterface feesCalculatorInterface = new FeesCalculator01();
        return feesCalculatorInterface.calculateRentalFees(rentalBike);
    }


    public void updateDataAfterReturnBike() {
        System.out.println("Gọi đến các hàm update của trả xe");
        Bike.lockBike(rentalBike.getBike().getBarcode(),stationSelected.getStationId());
        Station.updateStationAfterReturnBike(stationSelected.getStationId(),rentalBike.getBike().getType());
        RentalBike.updateRentalBike();
    }
}
