package controller;

import exceptions.PaymentException;
import model.Card;
import subsystem.PayInterbankInterface;
import subsystem.PayInterbankSubsystem;

public class PayController extends BasePayment {

	static {
		PaymentFactory.register(PaymentConfig.PAY, new PayController());
	}
	
	private PayInterbankInterface interbank;
	
	public PayController() {
		interbank = new PayInterbankSubsystem();
	}
	
	public BasePayment create() {
		return new PayController();
	}

	public void request(Card card, int amount, String message) throws PaymentException {
		interbank.requestToPay(card, amount, message);
		
	}

}
