package controller;

import exceptions.PaymentException;
import model.Card;

public abstract class BasePayment {
	public abstract BasePayment create();

	public abstract void request(Card card, int amount, String message) throws PaymentException;
}
