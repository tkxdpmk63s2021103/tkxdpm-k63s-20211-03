package controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

public class Main extends Application {

    public SQLServerDataSource getConnect(){
        SQLServerDataSource ds = new SQLServerDataSource();
        ds.setUser("sa");
        ds.setPassword("Meyeuthuong123");
        ds.setServerName("localhost");
        ds.setPortNumber(1433);
        ds.setDatabaseName("Ecobike");
        return ds;
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/home/HomeScreen.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Ecobike");
        primaryStage.show();
    }
}
