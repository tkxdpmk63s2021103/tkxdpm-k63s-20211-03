package controller;

import exceptions.PaymentException;
import model.Card;

public class PaymentController extends BaseController {
	
	private Card card;
	
	public void request(int payType, int amount, String message) throws PaymentException {
		BasePayment controller = PaymentFactory.create(payType);
		controller.request(card, amount, message);
	}
	
	public void setCard(Card card) {
		this.card = card;
	}
	
}

