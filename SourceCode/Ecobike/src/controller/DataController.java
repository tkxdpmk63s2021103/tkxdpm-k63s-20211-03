package controller;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import model.*;

import java.util.ArrayList;

public class DataController {
    private SQLServerDataSource ds = new Main().getConnect();


    public Station getStationFromId(int stationId){
       return Station.getStationFromId(stationId);
    }

    public  ArrayList<Station> getListStation(){
        return Station.getListStation();
    }

    public ArrayList<Station> searchStation(String input){
        return Station.searchStation((input));
    }

    public ArrayList<Bike> getBikeFromStation(int stationId) {
        return Bike.getBikeFromStation(stationId);
    }

    public Bike getBikeFromBarcode(String barcode){
        return Bike.getBikeFromBarcode(barcode);
    }

	public Card getCardFromCardNumber(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	public ArrayList<Bike> getAllBikes() {
		return Bike.getAllBikes();
	}
   
}
