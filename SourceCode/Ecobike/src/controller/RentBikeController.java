package controller;


import controller.CalculatorLogic.FeesCalculator01;
import controller.CalculatorLogic.FeesCalculatorInterface;
import model.Bike;
import model.RentalBike;
import model.Station;
import static view.viewmuonxe.BikeDetailsHandle.bikeSelected;


public class RentBikeController {

    public int calculateDeposit(Bike bike){
        FeesCalculatorInterface feesCalculatorInterface = new FeesCalculator01();
        return feesCalculatorInterface.getDeposit(bike.getType());
    }

    public void updateDataAfterRentBike(){
        System.out.println("Gọi đến các hàm update của mượn xe");
        Bike.unlockBike(bikeSelected.getBarcode());
        Station.updateStationAfterRentBike(bikeSelected.getStationId(),bikeSelected.getType());
        RentalBike.saveRentalBike();
    }



}
