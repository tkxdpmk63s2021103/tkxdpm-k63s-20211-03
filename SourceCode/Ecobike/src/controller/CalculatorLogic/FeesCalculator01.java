package controller.CalculatorLogic;


import model.RentalBike;

import java.util.Date;

public class FeesCalculator01 implements FeesCalculatorInterface {


	@Override
	public int calculateRentalFees(RentalBike rentalBike) {
		int phiThueXe = 0;
		Date date = new Date();
		System.out.println(rentalBike.getStartTime().getYear()+" "+rentalBike.getStartTime().getMonth()+" "+rentalBike.getStartTime().getDate()+" "+rentalBike.getStartTime().getHours()+" "+rentalBike.getStartTime().getMinutes()+" "+rentalBike.getStartTime().getSeconds());
		System.out.println(rentalBike.getStartTime().getTime());
		System.out.println(rentalBike.getStartTime().toString());


		System.out.println(date.getYear()+" "+date.getMonth()+" "+date.getDate()+" "+date.getHours()+" "+date.getMinutes()+" "+date.getSeconds());
		System.out.println(date.getTime());
		System.out.println(date.toString());
		long hieu = (date.getTime() - rentalBike.getStartTime().getTime())/1000;
		int phut = (int) hieu/60;
		if(phut <= 10){
			System.out.println("Không cần thanh toán");
		}else{
			if(phut<=30){
				phiThueXe = 10000;

			}else{
				phiThueXe = ((phut-30)/15)*3000 + 10000;
			}
		}
		int tienCoc = 0;
		if(rentalBike.getBike().getType().equals("SingleBike")){
			tienCoc = 400000;
		}else if(rentalBike.getBike().getType().equals("EBike")){
			tienCoc = 700000;
		}else if(rentalBike.getBike().getType().equals("TwinBike")){
			tienCoc = 550000;
		}

		return phiThueXe - tienCoc;
	}

	@Override
	public int getDeposit(String bikeType) {
		if(bikeType.equals("SingleBike")){
			return 400000;
		} else if(bikeType.equals("EBike")){
			return 700000;
		} else if(bikeType.equals("TwinBike")){
			return 550000;
		} else{
			return -1;
		}
	}
}
