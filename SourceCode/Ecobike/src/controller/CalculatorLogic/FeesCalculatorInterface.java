package controller.CalculatorLogic;

import model.RentalBike;

public interface FeesCalculatorInterface {

	public int calculateRentalFees(RentalBike rentalBike);
	public int getDeposit(String bikeType);
}
