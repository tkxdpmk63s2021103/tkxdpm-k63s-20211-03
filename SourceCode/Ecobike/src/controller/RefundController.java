package controller;

import exceptions.PaymentException;
import model.Card;
import subsystem.RefundInterbankInterface;
import subsystem.RefundInterbankSubsystem;

public class RefundController extends BasePayment{

	static {
		PaymentFactory.register(PaymentConfig.REFUND, new RefundController());
	}
	
	private RefundInterbankInterface interbank;
	
	public RefundController() {
		interbank = new RefundInterbankSubsystem();
	}
	
	public BasePayment create() {
		return new RefundController();
	}

	public void request(Card card, int amount, String message) throws PaymentException {
		interbank.requestToRefund(card, amount, message);
	}

}
