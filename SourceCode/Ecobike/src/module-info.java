module Ecobike {
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.base;
    requires com.microsoft.sqlserver.jdbc;
    requires java.sql;
    requires java.naming;


    opens controller;
//    opens controller.CalculatorLogic;
    opens view.stationdetails;
    opens view.viewmuonxe;
    opens view.viewtraxe;
    opens view.admin;
    opens model;
    opens view.home;
    opens view.Pay;
    opens view.bikemanagement;
    opens view.bikemanagement.bikeeditor;
    opens view.bikemanagement.bikeadd;
}