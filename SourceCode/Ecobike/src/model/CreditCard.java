package model;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import controller.Main;
import exceptions.PaymentException;
import view.Pay.CreditCardDateValidation;

public class CreditCard extends Card{
	private String cardHolderName;
    private String cardNumber;
    private int bankId;
    private String expirationDate;
    private int securityCode;
    
    
    private static SQLServerDataSource ds = new Main().getConnect();

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) throws PaymentException {
    	this.expirationDate = CreditCardDateValidation.validate(expirationDate);
    }

    public int getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(int securityCode) {
        this.securityCode = securityCode;
    }

//    public Card getCardFromCardNumber(String cardNumber) {
//        Card card = new Card();
//        try {
//            Connection cnn = ds.getConnection();
//            Statement statement = cnn.createStatement();
//            ResultSet resultSet = statement.executeQuery(
//            		"select  c a r d h o l d e r N a m e ,"
//            		+ "c a r d N u m b e r ,"
//            		+ " e x p i r a t i o n D a t e ,"
//            		+ " s e c u r i t y C o d e , "
//            		+ "b a n k I d ,"
//            		+ " m o n e y "
//            		+ "from card where cardNumber = '"+ cardNumber +"'");
//
//            while (resultSet.next()) {
//                card.setCardholderName(resultSet.getString(1));
//                card.setCardNumber(resultSet.getString(2));
//                card.setExpirationDate(resultSet.getString(3));
//                card.setSecurityCode(resultSet.getString(4));
//                card.setBankId(resultSet.getInt(5));
//                card.setMoney(resultSet.getInt(6));
//                break;
//            }
//        } catch (SQLServerException throwables) {
//            throwables.printStackTrace();
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//        return card;
//    }    
	    
}
