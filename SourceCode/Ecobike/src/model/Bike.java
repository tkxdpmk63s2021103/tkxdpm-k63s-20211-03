package model;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import controller.Main;
import exceptions.AddBikeInitException;
import exceptions.ConflictBikeInfoException;
import exceptions.InvalidBikeInfoException;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class Bike {
	private String barcode;
	private String name;
	private String type;
	private int weight;
	private String licensePlate;
	private Date manufacturingDate;
	private String producer;
	private int cost;
	private String description;
	private String imgPath;
	private int stationId;
	private String stationName;

	private static SQLServerDataSource ds = new Main().getConnect();

	public Bike() {
	}

	public int getStationId() {
		return stationId;
	}

	public void setStationId(int stationId) {
		this.stationId = stationId;
	}

	public Bike(String type) {
		this.type = type;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public Date getManufacturingDate() {
		return manufacturingDate;
	}

	public void setManufacturingDate(Date manufacturingDate) {
		this.manufacturingDate = manufacturingDate;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public static ArrayList<Bike> getBikeFromStation(int stationId) {
		ArrayList<Bike> bikes = new ArrayList<>();
		Bike bike = null;
		try {
			Connection cnn = ds.getConnection();
			Statement statement = cnn.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from bike where stationId = '" + stationId + "'");

			while (resultSet.next()) {
				String typeBike = resultSet.getString(3);
				if (typeBike.equals("EBike")) {
					bike = new EBike();
				} else if (typeBike.equals("TwinBike")) {
					bike = new TwinBike();
				} else {
					bike = new SingleBike();
				}
				bike.setBarcode(resultSet.getString(1));
				bike.setName(resultSet.getString(2));
				bike.setWeight(resultSet.getInt(4));
				bike.setLicensePlate(resultSet.getString(5));
				bike.setManufacturingDate(resultSet.getDate(6));
				bike.setProducer(resultSet.getString(7));
				bike.setCost(resultSet.getInt(8));
				bike.setDescription(resultSet.getString(9));
				bike.setImgPath(resultSet.getString(10));
				bike.setStationId(resultSet.getInt(11));
				bikes.add(bike);
			}
		} catch (SQLServerException throwables) {
			throwables.printStackTrace();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return bikes;
	}

	public static Boolean deleteBikeFromBarcode(String barcode) {
		try {
			Connection cnn = ds.getConnection();
			Statement statement = cnn.createStatement();
			String sql = "delete from bike where barcode = '" + barcode + "'";
			int rowChange = statement.executeUpdate(sql);
			if (rowChange > 0) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public static Bike getBikeFromBarcode(String barcode) {
		Bike bike = null;
		try {
			Connection cnn = ds.getConnection();
			Statement statement = cnn.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from bike where barcode = '" + barcode + "'");

			while (resultSet.next()) {
				String typeBike = resultSet.getString(3);
				if (typeBike.equals("EBike")) {
					bike = new EBike();
				} else if (typeBike.equals("TwinBike")) {
					bike = new TwinBike();
				} else {
					bike = new SingleBike();
				}
				bike.setBarcode(resultSet.getString(1));
				bike.setName(resultSet.getString(2));
				bike.setWeight(resultSet.getInt(4));
				bike.setLicensePlate(resultSet.getString(5));
				bike.setManufacturingDate(resultSet.getDate(6));
				bike.setProducer(resultSet.getString(7));
				bike.setCost(resultSet.getInt(8));
				bike.setDescription(resultSet.getString(9));
				bike.setImgPath(resultSet.getString(10));
				bike.setStationId(resultSet.getInt(11));
			}
		} catch (SQLServerException throwables) {
			throwables.printStackTrace();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return bike;
	}

	public static void unlockBike(String barcode) {
		try {
			Connection cnn = ds.getConnection();
			PreparedStatement preparedStatement = cnn
					.prepareStatement("update Bike set stationId = NULL where barcode = '" + barcode + "'");
			preparedStatement.execute();
		} catch (SQLServerException throwables) {
			throwables.printStackTrace();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
	}

	public static void lockBike(String barcode, int stationId) {
		try {
			Connection cnn = ds.getConnection();
			PreparedStatement preparedStatement = cnn.prepareStatement(
					"update Bike set stationId = '" + stationId + "' where barcode = '" + barcode + "'");
			preparedStatement.execute();
		} catch (SQLServerException throwables) {
			throwables.printStackTrace();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
	}

	/**
	 * Get all bike in database
	 * 
	 * @return {ArrayList<Bike>} Bike List
	 */
	public static ArrayList<Bike> getAllBikes() {
		ArrayList<Bike> bikes = new ArrayList<>();
		Bike bike = null;
		try {
			Connection cnn = ds.getConnection();
			Statement statement = cnn.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from bike");

			while (resultSet.next()) {
				String typeBike = resultSet.getString(3);
				if (typeBike.equals("EBike")) {
					bike = new EBike();
				} else if (typeBike.equals("TwinBike")) {
					bike = new TwinBike();
				} else {
					bike = new SingleBike();
				}
				bike.setBarcode(resultSet.getString(1));
				bike.setName(resultSet.getString(2));
				bike.setWeight(resultSet.getInt(4));
				bike.setLicensePlate(resultSet.getString(5));
				bike.setManufacturingDate(resultSet.getDate(6));
				bike.setProducer(resultSet.getString(7));
				bike.setCost(resultSet.getInt(8));
				bike.setDescription(resultSet.getString(9));
				bike.setImgPath(resultSet.getString(10));
				bike.setStationId(resultSet.getInt(11));
				bikes.add(bike);
			}
		} catch (SQLServerException throwables) {
			throwables.printStackTrace();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return bikes;
	}

	/**
	 * Validate bike's basic info
	 * 
	 * @param bike
	 * @return true - Valid<br>
	 *         false - Invalid
	 * @throws Exception
	 */
	public static boolean validateBasicBikeInfo(Bike bike) throws InvalidBikeInfoException {
		if (bike.getBarcode() == null || bike.getBarcode().length() == 0)
			throw new InvalidBikeInfoException("Barcode không hợp lệ");
		if (bike.getCost() < 0)
			throw new InvalidBikeInfoException("Giá xe phải lớn hơn 0");
		if (bike.getLicensePlate() == null || bike.getLicensePlate().length() == 0)
			throw new InvalidBikeInfoException("Biển số xe không được bỏ trống");
		if (bike.getManufacturingDate() == null)
			throw new InvalidBikeInfoException("Ngày sản xuất không hợp lệ");
		if (bike.getName() == null || bike.getName().length() == 0)
			throw new InvalidBikeInfoException("Tên xe không hợp lệ");
		if (bike.getWeight() <= 0)
			throw new InvalidBikeInfoException("Khối lượng xe phải lớn hơn 0");
		if (bike.getProducer() == null || bike.getProducer().length() == 0)
			throw new InvalidBikeInfoException("Không được bỏ trống nhà sản xuất");
		if (bike.getType() == null || (bike.getType().compareTo("EBike") != 0
				&& bike.getType().compareTo("TwinBike") != 0 && bike.getType().compareTo("SingleBike") != 0))
			throw new InvalidBikeInfoException("Loại xe không hợp lệ");
		if (LocalDate.now().compareTo(bike.getManufacturingDate().toLocalDate()) < 0)
			throw new InvalidBikeInfoException("Ngày sản xuất không thể sau ngày hiện tại");
		if (bike.getStationId() == 0)
			throw new InvalidBikeInfoException("Không thể chuyển sang trạng thái đang thuê");
		return true;
	}

	/**
	 * Kiểm tra thông tin xe với cơ sở dữ liệu
	 * 
	 * @return
	 * @throws SQLException
	 * @throws ConflictBikeInfoException
	 * 
	 */
	public static boolean validateTemporalEditedBikeInfo(Bike bike, int oldStationId)
			throws ConflictBikeInfoException, SQLException {
		// Kiểm tra lặp biển số xe
		if (Bike.isLicensePlateDuplicate(bike.getBarcode(), bike.getLicensePlate()))
			throw new ConflictBikeInfoException("Xe trùng biển số");
		// Kiểm tra bãi xe mới có tồn tại và con chỗ trống
		Bike.isValidStation(bike.getStationId(), oldStationId);
		// Kiểm tra xe có đang được thuê
		if (Bike.isRenting(bike.getBarcode()))
			throw new ConflictBikeInfoException("Xe đang được thuê");
		return true;
	}
	
	public static boolean validateTemporalNewBikeInfo(Bike bike) throws ConflictBikeInfoException, SQLException {
		// Kiểm tra xem biển số có bị trùng
		if (Bike.isLicensePlateDuplicate(bike.getBarcode(), bike.getLicensePlate()))
			throw new ConflictBikeInfoException("Xe trùng biển số");
		// Kiểm tra xem bãi xe có tồn tại và còn chỗ trống
		Bike.isValidStation(bike.getStationId(), 0);
		return true;
	}

	/**
	 * Kiểm tra biển số xe có trùng với xe nào ngoài xe cần thay đổi thông tin không
	 * 
	 * @param barcode      Mã xe
	 * @param licensePlate Biển số xe
	 * @return
	 * @throws SQLException
	 */
	public static boolean isLicensePlateDuplicate(String barcode, String licensePlate) throws SQLException {
		Connection cnn = ds.getConnection();
		Statement statement = cnn.createStatement();
		// Biển số của xe khác trùng với giá trị xe hiện tại
		ResultSet resultSet = statement.executeQuery(
				"SELECT * FROM BIKE WHERE licensePlate='" + licensePlate + "' AND NOT barcode='" + barcode + "'");
		// Nếu tìm thấy, resultSet.next() trả về giá trị true, và ngược lại
		return resultSet.next();
	}

	/**
	 * Kiểm tra xe có đang được thuê, nếu xe đang thuê thì stationId = null, khi
	 * getInt trả về 0
	 * 
	 * @param barcode Barcode của xe
	 * @return
	 * @throws SQLException
	 */
	public static boolean isRenting(String barcode) throws SQLException {
		Connection cnn = ds.getConnection();
		Statement statement = cnn.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT stationId FROM BIKE WHERE barcode='" + barcode + "'");
		if (!resultSet.next())
			throw new SQLException("Không tìm thấy barcode của xe trong hệ thống");
		else {
			int stationId = resultSet.getInt("stationId");
			if (stationId == 0)
				return true;
			else
				return false;
		}
	}

	/**
	 * Kiểm tra xem barcode đã được sử dụng?
	 * 
	 * @param barcode Barcode của xe
	 * @return
	 * @throws SQLException
	 * @throws ConflictBikeInfoException
	 */
	public static boolean isBikeExist(String barcode) throws SQLException, ConflictBikeInfoException {
		Connection cnn = ds.getConnection();
		Statement statement = cnn.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT * FROM BIKE WHERE barcode='" + barcode + "'");
		if (resultSet.next())
			throw new ConflictBikeInfoException("Barcode đã được liên kết với một xe khác, vui lòng thử lại");
		return true;
	}

	/**
	 * Kiểm tra trạm xe có tồn tại, hay nếu chuyển còn chỗ trống?
	 * 
	 * @param stationId    Id bãi xe mới
	 * @param oldStationId Id bãi xe cũ, trong trường hợp thêm xe, truyền 0
	 * @return
	 * @throws SQLException
	 */
	public static boolean isValidStation(int stationId, int oldStationId)
			throws ConflictBikeInfoException, SQLException {
		Connection cnn = ds.getConnection();
		Statement statement = cnn.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT * FROM station WHERE stationId=" + stationId);
		if (!resultSet.next())
			throw new ConflictBikeInfoException("Bãi xe không tồn tại.");
		else if (stationId != oldStationId && resultSet.getInt("numberOfEmptyDocks") < 1) {
			throw new ConflictBikeInfoException("Bãi xe đã hết chỗ");
		}
		return true;
	}

	/**
	 * Lưu thông tin xe
	 * 
	 * @param bike
	 * @return
	 * @throws SQLException
	 */
	public static int saveBike(Bike bike) throws SQLException {
		Connection cnn = ds.getConnection();
		Statement statement = cnn.createStatement();
		String query = "UPDATE BIKE SET name='" + bike.getName() + "',type='" + bike.getType() + "',weight="
				+ bike.getWeight() + ",licensePlate='" + bike.getLicensePlate() + "',manuafacturingDate='"
				+ bike.getManufacturingDate().toString() + "',producer='" + bike.getProducer() + "',cost="
				+ bike.getCost() + ", description='" + bike.getDescription() + "', stationId=" + bike.getStationId()
				+ " WHERE barcode='" + bike.getBarcode() + "'";
//		System.out.println(query);
		int result = statement.executeUpdate(query);
		return result;
	}

	public static String getNewBarcode() throws SQLException, AddBikeInitException {
		Connection cnn = ds.getConnection();
		Statement statement = cnn.createStatement();
		String query = "SELECT TOP 1 * FROM Bike ORDER BY barcode DESC";
		ResultSet resultSet = statement.executeQuery(query);
		if (!resultSet.next())
			return "00000001";
		else {
			String biggestBikeBarcode = resultSet.getString("barcode");
			int biggestBikeBarcodeInt = Integer.parseInt(biggestBikeBarcode);
			if (biggestBikeBarcodeInt >= 99999999)
				throw new AddBikeInitException("Số lượng xe đã đạt giới hạn");
			Integer newBikeBarcodeInt = biggestBikeBarcodeInt + 1;
			int countChar = newBikeBarcodeInt.toString().length();
			String newBikeBarcode = "";
			while (8 - countChar > 0) {
				newBikeBarcode += "0";
				countChar++;
			}
			return newBikeBarcode + newBikeBarcodeInt;
		}
	}

	public static int addBike(Bike bike) throws SQLException {
		Connection cnn = ds.getConnection();
		Statement statement = cnn.createStatement();
		String query = "INSERT INTO Bike (barcode, name, type, weight, licensePlate, manuafacturingDate, producer, cost, description, stationId) VALUES ('"
				+ bike.getBarcode() + "', '"
				+ bike.getName() + "', '"
				+ bike.getType() + "', "
				+ bike.getWeight() + ", '"
				+ bike.getLicensePlate() + "', '"
				+ bike.getManufacturingDate().toString() + "', '"
				+ bike.getProducer() + "', "
				+ bike.getCost() + ", '"
				+ bike.getDescription() + "', '"
				+ bike.getStationId() + "')";
		System.out.println(query);
		int result = statement.executeUpdate(query);
		return result;
	}
}
