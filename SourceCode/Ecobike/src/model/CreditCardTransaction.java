package model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import controller.Main;

public class CreditCardTransaction extends Transaction{
	private String cardNumber;
	private String cardHolder;
	
	private static SQLServerDataSource ds = new Main().getConnect();
	
	public CreditCardTransaction(int id, Date time, int amount, String message, String cardNumber, String cardHolder) {
		super(id, time, amount, message);
		this.cardNumber = cardNumber;
		this.cardHolder = cardHolder;
		
	}
	
	public void save() {
		try {
			Connection conn = ds.getConnection();
			PreparedStatement statement  = conn.prepareStatement("insert into giaoDich values (?, ?, ?, ?, ?, ?)");
			statement.setInt(1, getTransactionId());
			statement.setString(2, cardNumber);
			statement.setString(3, cardHolder);
			statement.setDate(4, getTransactionTime());
			statement.setInt(5, getTransactionValue());
			statement.setString(6, getTransactionMessage());
			System.out.println(statement);
			statement.execute();
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		
	}
}
