package model;

import controller.CalculatorLogic.FeesCalculator01;
import controller.CalculatorLogic.FeesCalculatorInterface;

public class TwinBike extends Bike {
    public TwinBike() {
        super("TwinBike");
    }
}
