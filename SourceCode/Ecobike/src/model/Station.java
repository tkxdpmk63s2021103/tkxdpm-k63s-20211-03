package model;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import controller.Main;

import java.sql.*;
import java.util.ArrayList;

public class Station {
    private int stationId;
    private String stationName;
    private String stationAddress;
    private int numberOfSingleBike;
    private int numberOfEBike;
    private int numberOfTwinBike;
    private int numberOfEmptyDocks;
    private static SQLServerDataSource ds = new Main().getConnect();

    public Station() {
    }

    public Station(int stationId, String stationName, String stationAddress, int numberOfSingleBike, int numberOfEBike, int numberOfTwinBike, int numberOfEmptyDocks) {
        this.stationId = stationId;
        this.stationName = stationName;
        this.stationAddress = stationAddress;
        this.numberOfSingleBike = numberOfSingleBike;
        this.numberOfEBike = numberOfEBike;
        this.numberOfTwinBike = numberOfTwinBike;
        this.numberOfEmptyDocks = numberOfEmptyDocks;
    }



    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStationAddress() {
        return stationAddress;
    }

    public void setStationAddress(String stationAddress) {
        this.stationAddress = stationAddress;
    }

    public int getNumberOfSingleBike() {
        return numberOfSingleBike;
    }

    public void setNumberOfSingleBike(int numberOfSingleBike) {
        this.numberOfSingleBike = numberOfSingleBike;
    }

    public int getNumberOfEBike() {
        return numberOfEBike;
    }

    public void setNumberOfEBike(int numberOfEBike) {
        this.numberOfEBike = numberOfEBike;
    }

    public int getNumberOfTwinBike() {
        return numberOfTwinBike;
    }

    public void setNumberOfTwinBike(int numberOfTwinBike) {
        this.numberOfTwinBike = numberOfTwinBike;
    }

    public int getNumberOfEmptyDocks() {
        return numberOfEmptyDocks;
    }

    public void setNumberOfEmptyDocks(int numberOfEmptyDocks) {
        this.numberOfEmptyDocks = numberOfEmptyDocks;
    }

    public static Station getStationFromId(int stationId){
        Station station = null;
        try {
            Connection cnn = ds.getConnection();
            Statement statement = cnn.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from station where stationId ='"+stationId+"'");
            while (resultSet.next()) {
                //int stationId, String stationName, String stationAddress, int numberOfSingleBike, int numberOfEBike, int numberOfTwinBike, int numberOfEmptyDocks
                station = new Station(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3),resultSet.getInt(4),resultSet.getInt(5),resultSet.getInt(6),resultSet.getInt(7));
            }
        } catch (SQLServerException throwables) {
            throwables.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return station;
    }
    public static ArrayList<Station> getListStation(){
        ArrayList<Station> stations = new ArrayList<>();
        try {
            Connection cnn = ds.getConnection();
            Statement statement = cnn.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from station");
            while (resultSet.next()) {
                //int stationId, String stationName, String stationAddress, int numberOfSingleBike, int numberOfEBike, int numberOfTwinBike, int numberOfEmptyDocks
                Station station = new Station(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3),resultSet.getInt(4),resultSet.getInt(5),resultSet.getInt(6),resultSet.getInt(7));
                stations.add(station);
            }
        } catch (SQLServerException throwables) {
            throwables.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return stations;
    }

    public static ArrayList<Station> searchStation(String input){
        ArrayList<Station> stations = new ArrayList<>();
        try {
            Connection cnn = ds.getConnection();
            Statement statement = cnn.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from station where stationName like N'%" + input + "%' or stationAddress like N'%" + input + "%'");
            while (resultSet.next()) {
                //int stationId, String stationName, String stationAddress, int numberOfSingleBike, int numberOfEBike, int numberOfTwinBike, int numberOfEmptyDocks
                Station station = new Station(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3),resultSet.getInt(4),resultSet.getInt(5),resultSet.getInt(6),resultSet.getInt(7));
                stations.add(station);
            }
        } catch (SQLServerException throwables) {
            throwables.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return stations;
    }

    public static void updateStationAfterRentBike(int stationId, String type){
        try {
            Connection cnn = ds.getConnection();
            PreparedStatement preparedStatement = cnn.prepareStatement("update station set numberOf"+type+" = numberOf"+type+" - 1, numberOfEmptyDocks = numberOfEmptyDocks + 1 where stationId =  "+stationId);
            preparedStatement.execute();
        } catch (SQLServerException throwables) {
            throwables.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void updateStationAfterReturnBike(int stationId, String type) {
        try {
            Connection cnn = ds.getConnection();
            PreparedStatement preparedStatement = cnn.prepareStatement("update station set numberOf"+type+" = numberOf"+type+" + 1, numberOfEmptyDocks = numberOfEmptyDocks - 1 where stationId =  "+stationId);
            preparedStatement.execute();
        } catch (SQLServerException throwables) {
            throwables.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
