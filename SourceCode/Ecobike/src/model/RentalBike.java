package model;


import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import controller.DataController;
import controller.Main;

import java.sql.*;
import java.util.Date;

import static view.viewmuonxe.BikeDetailsHandle.bikeSelected;
import static view.Pay.TestPayHandle.cardId;
import static view.viewtraxe.EnterBarcodeHandle.rentalBike;


public class RentalBike {
    private Bike bike;
    private Card card;
    private Date startTime;
    private Date endTime;
    private static SQLServerDataSource ds = new Main().getConnect();
    private static DataController dataController = new DataController();

    public RentalBike() {
    }



    public Bike getBike() {
        return bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public static RentalBike getRentalBikeFromBarcode(String barcode) {
        RentalBike rentalBike = new RentalBike();
        try {
            Connection cnn = ds.getConnection();
            Statement statement = cnn.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from RentalBike where barcode = '"+barcode+"' and dateReturnBike is null");

            while (resultSet.next()) {
               rentalBike.setBike(dataController.getBikeFromBarcode(resultSet.getString(2)));
//               rentalBike.setCard(dataController.getCardFromCardNumber(resultSet.getString(1)));
               rentalBike.setStartTime(resultSet.getDate(3));
               rentalBike.setEndTime(resultSet.getDate(4));
            }
        } catch (SQLServerException throwables) {
            throwables.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return rentalBike;
    }

    public static void saveRentalBike(){
        //Lưu RentalBike
        try (Connection connection = ds.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into RentalBike (carId,barcode,dateRentBike) values(?,?,?)");
            preparedStatement.setString(1, String.valueOf(cardId));
            preparedStatement.setString(2, bikeSelected.getBarcode());
            Date date = new Date();
            int year = date.getYear() + 1900;
            int month = date.getMonth() + 1;
            int day = date.getDate();
            System.out.println();
            preparedStatement.setString(3,year+"-"+month+"-"+day+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+".000" );
            preparedStatement.execute();
        } catch (SQLServerException throwables) {
            throwables.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void updateRentalBike() {
        Date date = new Date();
        int year = date.getYear() + 1900;
        int month = date.getMonth() + 1;
        int day = date.getDate();
        try {
            Connection cnn = ds.getConnection();
            PreparedStatement preparedStatement = cnn.prepareStatement("update RentalBike set dateReturnBike = '"+year+"-"+month+"-"+day+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+".000"+"' where barcode = '"+rentalBike.getBike().getBarcode()+"' and dateReturnBike is null");
            preparedStatement.execute();
        } catch (SQLServerException throwables) {
            throwables.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
