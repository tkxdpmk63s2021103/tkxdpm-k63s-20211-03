package model;

import java.sql.Date;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import controller.Main;

public abstract class Transaction {
	private int transactionId;
	private Date transactionTime;
	private int transactionValue;
	private String transactionMessage;
	
    public Transaction(int id, Date time, int amount, String message) {
    	this.transactionId = id;
    	this.transactionTime = time;
    	this.transactionValue = amount;
    	this.transactionMessage = message;
	}
    
	public int getTransactionId() {
		return transactionId;
	}

	public Date getTransactionTime() {
		return transactionTime;
	}

	public int getTransactionValue() {
		return transactionValue;
	}

	public String getTransactionMessage() {
		return transactionMessage;
	}

    
	public abstract void save();

}
