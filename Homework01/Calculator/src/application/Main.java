package application;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class Main extends Application {
	final public String APPLICATION_NAME = "Calculator";
	private GridPane root = new GridPane();
	private TextField firstNumber = new TextField();
	private Label additionLabel = new Label("+");
	private TextField secondNumber = new TextField();
	private Button calculateButton = new Button("=");
	private TextField result = new TextField();
	private Label error = new Label();

	public int getFirstNumber() {
		return Integer.parseInt(this.firstNumber.getText());
	}

	public int getSecondNumber() {
		return Integer.parseInt(this.secondNumber.getText());
	}

	public void setResult(int result) {
		this.result.setText(String.valueOf(result));
	}

	@Override
	public void start(Stage primaryStage) {
		// Stage and Window are counterparts,
		// as Stage and Window both have Title
		try {
			root.add(firstNumber, 1, 0);
			root.add(additionLabel, 0, 1);
			root.add(secondNumber, 1, 1);
			root.add(calculateButton, 0, 3);
			root.add(result, 1, 3);
			root.setVgap(3);
			root.setHgap(3);
			
			calculateButton.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					// Can't use this in anonymous class to refer to outer class
					root.getChildren().remove(error);
					try {
						setResult(getFirstNumber() + getSecondNumber());
					} catch (Exception e) {
						error.setText("Error: Please, input integer!");
						root.add(error, 1, 4);
					}
				}
			});

			Scene scene = new Scene(root, 200, 200);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

			primaryStage.setTitle(APPLICATION_NAME);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
