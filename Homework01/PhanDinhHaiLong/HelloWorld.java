package tkxdpm;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
public class HelloWorld extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {

		// New line
		Text inputText = new Text("Please enter your name");
		TextField inputField = new TextField();
		Button btn = new Button("Submit");
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				Alert alert = new Alert(AlertType.NONE);
				String username = inputField.getText();
				if (username.isEmpty()) {
					alert.setHeaderText("You haven't enter your name");	
				}
				else {
					alert.setHeaderText("Hello " + inputField.getText());					
				}
				alert.setTitle("Hello");
				ButtonType btn2 = new ButtonType("Close");
				alert.getButtonTypes().add(btn2);
				alert.showAndWait();
			}
			
		});

		FlowPane pane = new FlowPane();
		FlowPane.setMargin(inputText, new Insets(20, 0, 20, 20));
		pane.setHgap(25);
		
		pane.getChildren().add(inputText);
		pane.getChildren().add(inputField);
		pane.getChildren().add(btn);
		
		primaryStage.setTitle("Hello World");
		Scene scene = new Scene(pane, 500, 50);
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}
	
	public static void main(String[] args) {
		launch(args);
//		System.out.println("hello");
	}

}
